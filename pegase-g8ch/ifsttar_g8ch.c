/*
 * 
 * 	 Copyright (C)  2016 L.LEMARCHAND
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <linux/proc_fs.h>
#include <linux/cdev.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/uaccess.h>
#include "ifsttar_g8ch.h"
#include <linux/of_platform.h>
#include <linux/of_gpio.h>
#include <linux/spi/spi.h>
#include <linux/of_irq.h>
#include <linux/sched/signal.h>

MODULE_AUTHOR ("Arthur BOUCHE");
MODULE_DESCRIPTION ("IFSTTAR DRIVER G8CH");
MODULE_LICENSE ("GPL");
MODULE_VERSION ("0.1");

#define DRV_NAME		        "ifsttar-g8ch" 

#define CMD_READ(x)                 (0x80 | (x & 0x7F))
#define CMD_WRITE(x)                (0x00 | (x & 0x7F))

#define DRV_NAME_TRIG   "cfg8chTrig"
#define DRV_NAME_CAN 	"cfg8chCan"
#define DRV_NAME_PPS	"cfg8chgPPS"

#define MINOR_PPS  0
#define MINOR_CAN  1
#define MINOR_TRIG 2

#define VERSION_MAJ_AUTORISE    02
#define VERSION_MIN_AUTORISE    00



static bool			flag_it = false;
int					nbErrorOverFlow = 0;
static				DECLARE_WAIT_QUEUE_HEAD(ReadWait);
static struct		task_struct *taskReadFpga;
static bool			thread_read_running = false;

/*********PPS***********
 **********************/
int					ppsIsOpen = 0;
static				DEFINE_MUTEX(mutexPPSInfo);
struct				ppsInfos ppsInfoFromFpga;
static bool			flag_pps = false;
static				DECLARE_WAIT_QUEUE_HEAD(ReadPPS);


/*********CAN***********
 **********************/
static bool 		canIsOpen = false;
static bool 		canIsRunning = false;
/*static Date_S_NS_t timeToSendBuff_1;								// JGI - Correction Bugg buffer DMA
static char 	  *dataDmaToSendBuff_1 = NULL;						// JGI - Correction Bugg buffer DMA
static Date_S_NS_t timeToSendBuff_2;								// JGI - Correction Bugg buffer DMA
static char 	  *dataDmaToSendBuff_2 = NULL;						// JGI - Correction Bugg buffer DMA
static DEFINE_MUTEX(mutexDmaBuff_1);								// JGI - Correction Bugg buffer DMA
static DEFINE_MUTEX(mutexDmaBuff_2);*/								// JGI - Correction Bugg buffer DMA
static Date_S_NS_t	timeToSendBuff[NB_DMA_BUFF];					// JGI - Correction Bugg buffer DMA : Date (seconde et nanoseconde) du premier échantillon sauvegarder dans la table de buffers (dataDmaToSendBuff)
static char			*dataDmaToSendBuff[NB_DMA_BUFF];				// JGI - Correction Bugg buffer DMA : Echantillons sauvegardé dans la table de buffers
struct mutex		mutexDmaBuff[NB_DMA_BUFF];						// JGI - Correction Bugg buffer DMA : Sémaphore utilisé lors de l'écriture des données dans la table de buffers (dataDmaToSendBuff)
u16					index_dma_bw = INDEX_DMA_BUFF1;					// JGI - Correction Bugg buffer DMA : Indique le buffer de donnée à utilisé lors de la prochaine écriture de données d'acquisition analogique (index dma buffer write)
u16					index_dma_br = INDEX_DMA_BUFF1;					// JGI - Correction Bugg buffer DMA : Indique le buffer de donnée à utilisé lors de la prochaine lecture de données d'acquisition analogique (index dma buffer read)
static u32			nbDataDmaToRead = 10000;
static uint8_t		nb_channel;
static uint32_t		uiNbErrorOverFlow = 0;
/*static bool			flag_dma = false;*/							// JGI - Correction Bugg buffer DMA
static int			flag_dma = 0;									// JGI - Correction Bugg buffer DMA : Flag_dma, chaque bit correspond à un buffer. Bit0 => dataDmaToSendBuff[0], Bit1 => dataDmaToSendBuff[1]...
static				DECLARE_WAIT_QUEUE_HEAD(ReadDMA);
/*static int			lastDmaBuffRead = 0;*/						// JGI - Correction Bugg buffer DMA
u16					last_index_dma_br = INDEX_DMA_BUFF1;			// JGI - Correction Bugg buffer DMA : last_index_dma_br, index dernier buffer dataDmaToSendBuff[i] lu

/*********TRIG***********
 **********************/
static bool			trigIsOpen = false;
static bool			trigIsRunning = false;
static bool			flag_trig = false;
static char			*dataTrigToSendBuff = NULL;
static				DEFINE_MUTEX(mutexTrigBuff);
static				DECLARE_WAIT_QUEUE_HEAD(ReadTrig);
static				u16 countTrig;

struct				spi_device *spiHandler = NULL;
int					prog_done_level,fpga_reset_level,fpga_running_level,app_running_level,app_reset_level;
int					prog_done_gpios, hard_reset_fpga_gpios,app_reset_fpga_gpios;
struct				proc_dir_entry *basicProcfsEntry = NULL;
static dev_t		m_oDev;
unsigned int		cf8chMajor = 0;
static struct		class *m_oClass = NULL;
static struct		cdev m_oCDev;

//extern GpsData EXPORT_GD_DATA_FROM_SYNCHONIZATION_DRIVER (void);
//todo

static u16 cf8ch_read_register(int addr)
{
	ssize_t status;
	u16 result;
	u8 cmd = CMD_READ(addr);
	
	if (spiHandler == NULL) {
		pr_err("spiHandler is null !\n");
		return 0;
	}
	status = spi_write_then_read(spiHandler, &cmd, 1, &result, 2);
	if (status < 0) {
		pr_err("Spi read failed @addr = 0x%02X\n", addr);
		return 0;
	}
	return result;
}

static u32 cf8ch_read_register32(int addr)
{
	ssize_t status;
	u32 result;
	u8 cmd = CMD_READ(addr);
	if (spiHandler == NULL) {
		pr_err("spiHandler is null !\n");
		return 0;
	}
	status = spi_write_then_read(spiHandler, &cmd, 1, &result, 4);
	if (status < 0) {
		pr_err("Spi read32 failed @addr = 0x%02X\n", addr);
		return 0;
	}
	return result;
}

static u64 cf8ch_read_register64(int addr)
{
	ssize_t status;
	u64 result;
	u8 cmd = CMD_READ(addr);

	if (spiHandler == NULL) {
		pr_err("spiHandler is null !\n");
		return 0;
	}
	status = spi_write_then_read(spiHandler, &cmd, 1, &result, 8);
	if (status < 0) {
		pr_err("Spi read64 failed @addr = 0x%02X\n", addr);
		return 0;
	}
	return result;
}

static void cf8ch_write_register(int addr, u16 value)
{
	ssize_t status;
	u8 tx[3];

	if (spiHandler == NULL) {
		pr_err("spiHandler is null !\n");
		return;
	}
	tx[0] = CMD_WRITE(addr);
	tx[1] = (value >> 8) & 0xFF;
	tx[2] = value & 0xFF;

	status = spi_write(spiHandler, tx, 3);
	if (status < 0) {
		pr_err("Spi write failed @addr = 0x%02X\n", addr);
		return;
	}
}

static void cf8ch_write_register32(int addr, u32 value)
{
	ssize_t status;
	u8 tx[1 + sizeof(value)];

	if (spiHandler == NULL) {
		pr_err("spiHandler is null !\n");
		return;
	}
	tx[0] = CMD_WRITE(addr);
	// LSB register
	tx[1] = (value >> 8) & 0xFF;
	tx[2] = value & 0xFF;
	// MSB register
	tx[3] = (value >> 24) & 0xFF;
	tx[4] = (value >> 16) & 0xFF;
	status = spi_write(spiHandler, tx, sizeof(tx));
	if (status < 0) {
		pr_err("Spi write failed @addr = 0x%02X\n", addr);
		return;
	}
}
/*
static void cf8ch_write_register64(int addr, u64 value)
{
	ssize_t status;
	u8 tx[1 + sizeof(value)];

	if (spiHandler == NULL) {
		pr_err("spiHandler is null !\n");
		return;
	}
	tx[0] = CMD_WRITE(addr);
	// LSB register
	tx[1] = (value >> 8) & 0xFF;
	tx[2] = value & 0xFF;
	tx[3] = (value >> 24) & 0xFF;
	tx[4] = (value >> 16) & 0xFF;
	tx[5] = (value >> 40) & 0xFF;
	tx[6] = (value >> 32) & 0xFF;
	// MSB register
	tx[7] = (value >> 56) & 0xFF;
	tx[8] = (value >> 48) & 0xFF;
	status = spi_write(spiHandler, tx, sizeof(tx));
	if (status < 0) {
		pr_err("Spi write failed @addr = 0x%02X\n", addr);
		return;
	}
}*/

static inline u32 cf8ch_set_channels(u8 value)
{
	int i;
	u16 val = (u16) value & 0x00FF;
	nb_channel = 0;
	cf8ch_write_register(REGISTER_CHANNELS_CAN_ACTIVE, val);
	for (i = 0; i < 8 ; i++) { // 8 = nb sensor
		if ( (value & 0x01) == 1) {
			nb_channel++;
		}
		value = value >> 1;
	}
	return 0;
}

static inline void cf8ch_start_can(void)
{
	u32 reg;
	reg = cf8ch_read_register(REGISTER_CONTROL);
	reg |= CONTROL_BIT_ACQUISITION;
	cf8ch_write_register(REGISTER_CONTROL, reg);
}
static inline void cf8ch_stop_can(void)
{
	u32 reg;
	reg = cf8ch_read_register(REGISTER_CONTROL);
	reg &= ~CONTROL_BIT_ACQUISITION;
	cf8ch_write_register(REGISTER_CONTROL, reg);
}

static inline void cf8ch_stop_trig(void)
{
	u16 val = 0;
	cf8ch_write_register(REGISTER_CHANNELS_TRIG_ACTIVE, val);
}

static int cf8ch_read_fpga_pps (void)
{
	int ret = 0;
	if (mutex_lock_interruptible(&mutexPPSInfo)) {
		ret = -ERESTARTSYS;
	}
	ppsInfoFromFpga.qerr_beginning = cf8ch_read_register32(REGISTER_QERR);
	ppsInfoFromFpga.time = cf8ch_read_register64(REGISTER_TIME_EPOCH);
	ppsInfoFromFpga.period = cf8ch_read_register32(REGISTER_CAPTURE_FREQUENCY_PROCESS);
	ppsInfoFromFpga.qerr_end = 0;
	mutex_unlock(&mutexPPSInfo);
	
//	printk("ppsInfoCalc.qerr_beginning = %d\n",ppsInfoFromFpga.qerr_beginning);
//	printk(" ppsInfoCalc.time = %llu\n",ppsInfoFromFpga.time);
//	printk(" ppsInfoCalc.period = %d\n",ppsInfoFromFpga.period);

	flag_pps = true;
	wake_up_interruptible (&ReadPPS);
	return ret;
}

static int cf8ch_read_fpga_trig (void)
{
	int ret;
	ReadTrigChanelCompressed_t* dataTrigTemp;
	u16 count;
	u8 cmd;

	ret = 0;

	// Read data on FPGA
	if (spiHandler == NULL) {
		pr_err("spiHandler is null !\n");
		return -1;
	}

	// Read count value
	count = cf8ch_read_register(REGISTER_FIFO_COUNT);


	dataTrigTemp = NULL;
	dataTrigTemp = (ReadTrigChanelCompressed_t*)vmalloc(sizeof(ReadTrigChanelCompressed_t));
	if( dataTrigTemp == NULL ){
		ret = -EIO;
		goto exit;
	}
	dataTrigTemp->otherTrigs = NULL;
	if( count > 1 ){
		dataTrigTemp->otherTrigs = (ReadTrigChanel48_t*)vmalloc(sizeof(ReadTrigChanel48_t)*(count-1));
		if( dataTrigTemp->otherTrigs == NULL ){
			ret = -EIO;
			goto exit;
		}
	}

	// Read first sample
	cmd = CMD_READ(REGISTER_DATA_TRIG);
	ret = spi_write_then_read(spiHandler, &cmd, 1, &(dataTrigTemp->firstTrig), sizeof(ReadTrigChanel_t));
	if (ret < 0) {
		printk("\n\nRead first data trig failed!\n\n");
		ret = -EIO;
		goto exit;
	}

	// Read other samples
	if( count > 1 ){
		cmd = CMD_READ(REGISTER_BURST_TRIG);
		ret = spi_write_then_read(spiHandler, &cmd, 1, dataTrigTemp->otherTrigs, sizeof(ReadTrigChanel48_t)*(count-1));
		if (ret < 0) {
			printk("\n\nRead burst trig failed!\n\n");
			ret = -EIO;
			goto exit;
		}
	}

	// Lock mutex
	if (mutex_lock_interruptible(&mutexTrigBuff)) {
		ret = -ERESTARTSYS;
	}

	//copie data
	dataTrigToSendBuff = vmalloc(sizeof(ReadTrigChanel_t) + (sizeof(ReadTrigChanel48_t)*(count-1)));
	if( dataTrigToSendBuff == NULL ){
		goto exit;
	}
	memcpy(dataTrigToSendBuff, &(dataTrigTemp->firstTrig), sizeof(ReadTrigChanel_t));
	if( count > 1 ){
		memcpy(dataTrigToSendBuff+sizeof(ReadTrigChanel_t), dataTrigTemp->otherTrigs, sizeof(ReadTrigChanel48_t)*(count-1));
	}
	countTrig = count;
	// Unlock mutex
	mutex_unlock(&mutexTrigBuff);


	//trig done
	cf8ch_read_register(REGISTER_IRQ_TRIG_DONE);

	//unlock the dma read
	flag_trig = 1;
	wake_up_interruptible (&ReadTrig);

	goto exit;

exit:

	if( dataTrigTemp->otherTrigs == NULL ){
		vfree(dataTrigTemp->otherTrigs);
		dataTrigTemp->otherTrigs = NULL;
	}
	if( dataTrigTemp != NULL ){
		vfree(dataTrigTemp);
		dataTrigTemp = NULL;
	}

	return ret;
}

static int cf8ch_read_fpga_dma(void)
{
	u8 cmd;
	int rc = 0;
	static char *dataDmaTemp;
	Date_S_NS_t timeToSendTemp;
	struct spi_transfer transfer[2];

	if (signal_pending(current)) {
		return -EINTR;
	}
	// Spi is allocated
	if (spiHandler == NULL) {
		pr_err("Driver 8ch : spiHandler is null !\n");
		return -1;
	}
	//printk("spi ok\n");
	dataDmaTemp = NULL;
	if(( nbDataDmaToRead == 0 ) || ( nb_channel == 0 )) {
		return -1;
	}
	dataDmaTemp = kmalloc(nbDataDmaToRead * sizeof(uint32_t) * nb_channel,GFP_DMA);
	if( dataDmaTemp == NULL ) {
		printk("Driver 8ch : ERROR can't allocate dataDmaTemp\n");
		return -1;
	}
	//printk("kmalloc ok\n");
	cmd = CMD_READ(REGISTER_BURST_CAN);
	memset(transfer, 0, sizeof(transfer));
	transfer[0].tx_buf = &cmd;
	transfer[0].len = sizeof(cmd);
	transfer[1].rx_buf = dataDmaTemp;
	transfer[1].len = nbDataDmaToRead * sizeof(uint32_t) * nb_channel;

	rc = spi_sync_transfer(spiHandler, &transfer[0], 2);
	if (rc < 0) {
		pr_err("Driver 8ch : Spi read Data failed\n");
		kfree(dataDmaTemp);
		return -1;
	}
	//printk("spi_sync_transfer ok\n");
	cmd = CMD_READ(REGISTER_DATE_CAN);
	rc = spi_write_then_read(spiHandler, &cmd, 1, (char *)&timeToSendTemp, sizeof(Date_S_NS_t));
	if (rc < 0) {
		pr_err("Driver 8ch : ERROR Spi read time failed\n");
		kfree(dataDmaTemp);
		return -1;
	}
	//printk("spi_write_then_read ok\n");
	// JGI - Correction Bugg buffer DMA
	if (((flag_dma == 0) && (index_dma_bw == index_dma_br)) ||			// Si les buffers DMA sont tous vides (disponibles pour une récupération de données)
		((flag_dma != 0) && (index_dma_bw != index_dma_br))) {			// Si une partie des buffers DMA sont vides (disponibles pour une récupération de données)

		// Lock mutex
		if (mutex_lock_interruptible(&mutexDmaBuff[index_dma_bw])) {
			kfree(dataDmaTemp);
			return -ERESTARTSYS;
		}

		// Copie time
		timeToSendBuff[index_dma_bw] = timeToSendTemp;

		dataDmaToSendBuff[index_dma_bw] = vmalloc(nbDataDmaToRead * sizeof(uint32_t) * nb_channel);
		if( dataDmaToSendBuff[index_dma_bw] == NULL ) {
			printk("Driver 8ch : ERROR can't allocate dataDmaToSendBuff[%d]\n", index_dma_bw);
		}

		// Copie data
		memcpy(dataDmaToSendBuff[index_dma_bw], dataDmaTemp, nbDataDmaToRead * sizeof(uint32_t)*nb_channel);

		flag_dma |= (1 << index_dma_bw);																// Activation du flag correspondant au buffer utilisé

		mutex_unlock(&mutexDmaBuff[index_dma_bw]);

		index_dma_bw++;																					// Déplacement de l'index au buffer suivant pour la prochaine récupération de données
		if (index_dma_bw >= NB_DMA_BUFF) {
			index_dma_bw = INDEX_DMA_BUFF1;
		}

	} else if ((flag_dma != 0) && (index_dma_bw == index_dma_br)) {											// Si les buffers DMA sont tous remplis (indisponibles pour une récupération de données => Perte des données les plus récentes tant que les buffers ne sont lues et disponibles pour nouvelle écriture)
//		printk("ERROR : réécriture du buffer dma\n");
		printk("Driver 8ch : ERROR The %d buffers dma are full\n", NB_DMA_BUFF);
		++uiNbErrorOverFlow;		
	} else {	// flag_dma == 0 && index_dma_bw != index_dma_br											// Cas anormal
		printk("Driver 8ch : ERROR Anormal dma buffer write state\n");
	}
	//printk("after wake_up_interruptible  ok\n");
	wake_up_interruptible (&ReadDMA);
	kfree(dataDmaTemp);
	
	cf8ch_read_register32(REGISTER_DMA_DONE);
	//printk("after cf8ch_read_register32 DMA  ok\n");
	return 0;
}

static irqreturn_t cf8ch_irq_fpga (int irq, void *dev_id)
{
	flag_it = true;
	wake_up_interruptible(&ReadWait);
	//printk("IRQ\n");
	return IRQ_HANDLED;
}

static int cf8ch_thread_read(void *data)
{
	u16 type_irq;
	int ret;
	while (thread_read_running) {
		ret = wait_event_interruptible_timeout(ReadWait, flag_it == true, HZ * 2);
		flag_it = false;

		type_irq = cf8ch_read_register(REGISTER_TYPE_IRQ);
		//printk(" type_irq = %d\n",type_irq);
		if ( (type_irq & 0b0000000000000001) == 0b0000000000000001) {
			cf8ch_read_fpga_dma();
		}
		if ( (type_irq & 0b0000000000000010) == 0b0000000000000010) {
			cf8ch_read_fpga_pps();
		}
		if ( (type_irq & 0b0000000000000100) == 0b0000000000000100) {
			cf8ch_read_fpga_trig();
		} else {
			pr_debug(" type_irq = %d ret = %d\n", type_irq,ret);
		}

	}
	return 0;
}

static long cf8ch_ioctl (struct file * filp, unsigned int cmd, unsigned long arg)
{
	u64 u64_temp;
	u32 u32_temp;
	u16 u16_temp;
	u8 u8_temp;
	int ret = 0;
	switch(cmd)
	{
		// Affiche la commande passée ainsi que l'argument
		case CF8CH_ENABLE_CAN_ACQUISITION:
			cf8ch_start_can();
			canIsRunning = true;
			break;

		case CF8CH_DISABLE_CAN_ACQUISITION:
			u32_temp = cf8ch_read_register(REGISTER_CONTROL);
			u32_temp &= ~CONTROL_BIT_ACQUISITION;
			cf8ch_write_register(REGISTER_CONTROL, u32_temp);
			canIsRunning = false;
			break;

		case CF8CH_GET_NB_DATA_DMA:
			u32_temp = cf8ch_read_register32(REGISTER_NB_DATA_DMA_RETURN);
			ret = copy_to_user((u32 *) arg, &(u32_temp),sizeof(u32));
			break;

		case CF8CH_SET_NB_DATA_DMA:
			ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
			nbDataDmaToRead = u32_temp;
			cf8ch_write_register32(REGISTER_NB_DATA_DMA_RETURN, u32_temp);
			break;

		case CF8CH_GET_CAN_CHANNELS:
			u16_temp = cf8ch_read_register(REGISTER_CHANNELS_CAN_ACTIVE);
			ret = copy_to_user((u16 *) arg, &(u16_temp),sizeof(u16));
			break;

		case CF8CH_SET_CAN_CHANNELS:
			ret = copy_from_user(&u8_temp, (u8 *) arg, sizeof(u8));
			cf8ch_set_channels(u8_temp);
			break;

		case CF8CH_ENABLE_TRIG_ACQUISITION:
			cf8ch_write_register(REGISTER_TRIG_CONTROL, CONTROL_BIT_ACQUISITION);
			trigIsRunning = true;
			break;

		case CF8CH_DISABLE_TRIG_ACQUISITION:
			cf8ch_write_register(REGISTER_TRIG_CONTROL, 0);
			trigIsRunning = false;
			break;

		case CF8CH_GET_CHANELS_TRIG:
			u16_temp = cf8ch_read_register(REGISTER_CHANNELS_TRIG_ACTIVE);;
			ret = copy_to_user((u16 *) arg, &(u16_temp),sizeof(u16));
			break;

		case CF8CH_SET_CHANELS_TRIG:
			ret = copy_from_user(&u8_temp, (u8 *) arg, sizeof(u8));
			u16_temp = (u16) u8_temp & 0x00FF;
			cf8ch_write_register(REGISTER_CHANNELS_TRIG_ACTIVE, u16_temp);
			break;

		case CF8CH_GET_STATUS_TRIG_OFF :
			u32_temp = cf8ch_read_register(REGISTER_STATUS_TRIG_OFF);
			ret = copy_to_user((u32 *) arg, &(u32_temp),sizeof(u32));
			break;

		case CF8CH_RESET_TRIG_OFF:
			ret = cf8ch_read_register32(REGISTER_TRIG_OFF_RST);
			break;

/*		case CF8CH_GET_CAN_FE_DIVISOR:
			u32_temp = cf8ch_read_register32(REGISTER_CAN_FE_DIVISOR);
			ret = copy_to_user((u32 *) arg, &(u32_temp),sizeof(u32));
			break;
			
		case CF8CH_SET_CAN_FE_DIVISOR:
			ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
			cf8ch_write_register32(REGISTER_CAN_FE_DIVISOR, u32_temp);			
			break;*/

		case CF8CH_GET_CAN_FE:
			u32_temp = cf8ch_read_register32(REGISTER_CAN_FE);
			ret = copy_to_user((u32 *) arg, &(u32_temp),sizeof(u32));
			break;
			
		case CF8CH_SET_CAN_FE:
			ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
			cf8ch_write_register32(REGISTER_CAN_FE, u32_temp);
			break;

		case CF8CH_GET_FPGA_VERSION:
			u32_temp =cf8ch_read_register(REGISTER_FPGA_VERSION);
			ret = copy_to_user((u32 *) arg, &(u32_temp),sizeof(u32));
			break;

		case CF8CH_GET_PLL_CAN_CLOCK:
			u32_temp = cf8ch_read_register32(REGISTER_PLL_CAN);
			ret = copy_to_user((u32 *) arg, &(u32_temp),sizeof(u32));
			break;

		case CF8CH_GET_FREQUENCE_TP2:
			u32_temp = cf8ch_read_register32(REGISTER_FREQUENCE_TP2);
			ret = copy_to_user((u32 *) arg, &(u32_temp),sizeof(u32));
			break;

		case CF8CH_SET_FREQUENCE_TP2:
			ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
			cf8ch_write_register32(REGISTER_FREQUENCE_TP2, u32_temp);
			break;

		case CF8CH_GET_CAN_FREQUENCY_PROCESS:
			u32_temp = cf8ch_read_register32(REGISTER_CAN_FREQUENCY_PROCESS);
			ret = copy_to_user((u32 *) arg, &(u32_temp),sizeof(u32));
			break;

		case CF8CH_GET_CAN_MODE:
			u16_temp = cf8ch_read_register(REGISTER_CAN_MODE);;
			ret = copy_to_user((u16 *) arg, &(u16_temp),sizeof(u16));
			break;
			
		case CF8CH_SET_CAN_MODE:
			ret = copy_from_user(&u16_temp, (u16 *) arg, sizeof(u16));
			cf8ch_write_register(REGISTER_CAN_MODE, u16_temp);
			break;

		case CF8CH_GET_CAN_CLK_PARAM_A:
			u16_temp = cf8ch_read_register(REGISTER_CAN_CLK_PARAM_A);
			ret = copy_to_user((u16 *) arg, &(u16_temp),sizeof(u16));
			break;

		case CF8CH_SET_CAN_CLK_PARAM_A:
			ret = copy_from_user(&u16_temp, (u16 *) arg, sizeof(u16));
			cf8ch_write_register(REGISTER_CAN_CLK_PARAM_A, u16_temp);
			break;

		case CF8CH_GET_CAN_CLK_PARAM_B:
			u16_temp = cf8ch_read_register(REGISTER_CAN_CLK_PARAM_B);
			ret = copy_to_user((u16 *) arg, &(u16_temp),sizeof(u16));
			break;

		case CF8CH_SET_CAN_CLK_PARAM_B:
			ret = copy_from_user(&u16_temp, (u16 *) arg, sizeof(u16));
			cf8ch_write_register(REGISTER_CAN_CLK_PARAM_B, u16_temp);
			break;

		case CF8CH_GET_CAN_CLK_PARAM_NB_CYCLE_A_HI:
			u16_temp = cf8ch_read_register(REGISTER_CAN_CLK_PARAM_NB_CYC_A_HI);
			ret = copy_to_user((u16 *) arg, &(u16_temp),sizeof(u16));
			break;

		case CF8CH_SET_CAN_CLK_PARAM_NB_CYCLE_A_HI:
			ret = copy_from_user(&u16_temp, (u16 *) arg, sizeof(u16));
			cf8ch_write_register(REGISTER_CAN_CLK_PARAM_NB_CYC_A_HI, u16_temp);
			break;

		case CF8CH_GET_CAN_CLK_PARAM_NB_CYCLE_A_LOW:
			u16_temp = cf8ch_read_register(REGISTER_CAN_CLK_PARAM_NB_CYC_A_LOW);
			ret = copy_to_user((u16 *) arg, &(u16_temp),sizeof(u16));
			break;

		case CF8CH_SET_CAN_CLK_PARAM_NB_CYCLE_A_LOW:
			ret = copy_from_user(&u16_temp, (u16 *) arg, sizeof(u16));
			cf8ch_write_register(REGISTER_CAN_CLK_PARAM_NB_CYC_A_LOW, u16_temp);
			break;

		case CF8CH_GET_CAN_CLK_PARAM_NB_CYCLE_B_HI:
			u16_temp = cf8ch_read_register(REGISTER_CAN_CLK_PARAM_NB_CYC_B_HI);
			ret = copy_to_user((u16 *) arg, &(u16_temp),sizeof(u16));
			break;

		case CF8CH_SET_CAN_CLK_PARAM_NB_CYCLE_B_HI:
			ret = copy_from_user(&u16_temp, (u16 *) arg, sizeof(u16));
			cf8ch_write_register(REGISTER_CAN_CLK_PARAM_NB_CYC_B_HI, u16_temp);
			break;

		case CF8CH_GET_CAN_CLK_PARAM_NB_CYCLE_B_LOW:
			u16_temp = cf8ch_read_register(REGISTER_CAN_CLK_PARAM_NB_CYC_B_LOW);
			ret = copy_to_user((u16 *) arg, &(u16_temp),sizeof(u16));
			break;

		case CF8CH_SET_CAN_CLK_PARAM_NB_CYCLE_B_LOW:
			ret = copy_from_user(&u16_temp, (u16 *) arg, sizeof(u16));
			cf8ch_write_register(REGISTER_CAN_CLK_PARAM_NB_CYC_B_LOW, u16_temp);
			break;

		case CF8CH_GET_TIMEOUT_TRIG:
			u16_temp = cf8ch_read_register(REGISTER_TIMEOUT_TRIG);
			ret = copy_to_user((u16 *) arg, &(u16_temp),sizeof(u16));
			break;
			
		case CF8CH_SET_TIMEOUT_TRIG:
			ret = copy_from_user(&u16_temp, (u16 *) arg, sizeof(u16));
			cf8ch_write_register(REGISTER_TIMEOUT_TRIG, u16_temp);
			break;

		case CF8CH_GET_NB_TRIG_OFF:
			u16_temp = cf8ch_read_register(REGISTER_SET_NB_TRIG_OFF);
			ret = copy_to_user((u16 *) arg, &(u16_temp),sizeof(u16));
			break;

		case CF8CH_SET_NB_TRIG_OFF:
			ret = copy_from_user(&u16_temp, (u16 *) arg, sizeof(u16));
			cf8ch_write_register(REGISTER_SET_NB_TRIG_OFF, u16_temp);
			break;

		case CF8CH_GET_NB_ERR_OF:
			u32_temp = uiNbErrorOverFlow;
			ret = copy_to_user((u32 *) arg, &(u32_temp),sizeof(u32));
			uiNbErrorOverFlow = 0;
			break;

		case CF8CH_GET_TIME_ACQUISITION_TS:
			u64_temp = timeToSendBuff[last_index_dma_br].ts;		// JGI - Correction Bugg buffer DMA : Acces direct with last_index_dma_br to the time timestamp
			ret = copy_to_user((u64 *) arg, &(u64_temp),sizeof(u64));
			break;

		case CF8CH_GET_TIME_ACQUISITION_MICRO:
			u32_temp = timeToSendBuff[last_index_dma_br].nanoS;		// JGI - Correction Bugg buffer DMA : Acces direct with last_index_dma_br to the time nanoseconde
			ret = copy_to_user((u32 *) arg, &(u32_temp),sizeof(u32));
			break;
		}

	return ret;
}

static ssize_t cf8ch_read_pps (struct file *filp, char *buf, size_t size, loff_t *offp){
	int ret;
	ret = 0;
	if(flag_pps == false){
		 wait_event_interruptible(ReadPPS, flag_pps != false);
	}
	flag_pps = false;
	if (mutex_lock_interruptible(&mutexPPSInfo)) {
		ret = -ERESTARTSYS;
	}
	ret = copy_to_user(buf, &ppsInfoFromFpga, sizeof(ppsInfoFromFpga));
	mutex_unlock(&mutexPPSInfo);
	return ret;
}

static ssize_t cf8ch_read_trig (struct file *filp, char *buf, size_t size, loff_t *offp)
{
	ssize_t nbData = 0;
	int status = 0;
	if (dataTrigToSendBuff == NULL ) {
		wait_event_interruptible(ReadTrig, flag_trig != 0);
	}
	flag_trig = 0;
	if (dataTrigToSendBuff != NULL) {
		if (mutex_lock_interruptible(&mutexTrigBuff)) {
			return -ERESTARTSYS;
		}
		status = copy_to_user(buf, dataTrigToSendBuff, (sizeof(ReadTrigChanel_t) + sizeof(ReadTrigChanel48_t) * (countTrig-1)));
		vfree(dataTrigToSendBuff);
		dataTrigToSendBuff = NULL;
		nbData = countTrig;
		mutex_unlock(&mutexTrigBuff);
	}else{
		return -1;
	}

	return nbData;
}

static ssize_t cf8ch_read_dma(struct file *filp, char *buf, size_t size, loff_t *offp)
{
	int status = 0;
	if (flag_dma == 0) {
		wait_event_interruptible(ReadDMA, flag_dma != 0);
		if (signal_pending(current)) {
			return -EINTR;
		}
	}

	// JGI - Correction Bugg buffer DMA
	// Lecture des données d'acquisition analogique dans les buffers dma
	if (((flag_dma != 0) && (index_dma_bw != index_dma_br)) ||				// Si une partie des buffers DMA sont remplis (données à lire disponibles)
		((flag_dma != 0) && (index_dma_bw == index_dma_br))) {				// Si les buffers DMA sont tous remplis (données à lire disponibles)

		if (mutex_lock_interruptible(&mutexDmaBuff[index_dma_br])) {
			return -ERESTARTSYS;
		}
		status = copy_to_user(buf, dataDmaToSendBuff[index_dma_br], ( size * sizeof(uint32_t) * nb_channel));
		vfree(dataDmaToSendBuff[index_dma_br]);
		dataDmaToSendBuff[index_dma_br] = NULL;

		last_index_dma_br = index_dma_br;									// Sauvegarde de l'index du buffer Dma qui vient d'être lue
		flag_dma &= ~(1 << index_dma_br);									// Desactivation du flag correspondant au buffer lu

		// Unlock mutex
		mutex_unlock(&mutexDmaBuff[index_dma_br]);

		index_dma_br++;														// Déplacement de l'index au buffer suivant pour la prochaine lecture de données
		if (index_dma_br >= NB_DMA_BUFF)
		{
			index_dma_br = INDEX_DMA_BUFF1;
		}

	} else if (flag_dma == 0) {												// Si les buffers DMA sont tous vide (pas de données à lire)
		printk("Driver 8ch : ERROR buffers dma read are empty\n");
	}
	else {
		printk("Driver 8ch : ERROR Anormal dma buffer read state\n");
		return -1;
	}
	
	return status;
}

static ssize_t cf8ch_read(struct file *filp, char *buf, size_t size, loff_t *offp)
{
	ssize_t rc;
	unsigned int minor = iminor(file_inode(filp));;

	if (minor == MINOR_CAN) {
		rc = cf8ch_read_dma(filp, buf, size, offp);
	} else if ( minor == MINOR_TRIG) {
		rc = cf8ch_read_trig(filp, buf, size, offp);
	}else if ( minor == MINOR_PPS) {
		rc = cf8ch_read_pps(filp, buf, size, offp);
	} else {
		pr_err("cf8ch_read : Wrong minor = %d\n",minor);
		return -1;
	}

	return rc;
}

static int cf8ch_open(struct inode *inode, struct file *filp)
{
	int status = 0;
	unsigned int minor = iminor(inode);
	switch (minor) {
	case MINOR_TRIG :
		if (trigIsOpen == true) {
			status = -EBUSY;
		} else {
			status = 0;
			trigIsOpen = true;
		}
		break;
	case MINOR_CAN :
		if (canIsOpen == true) {
			status = -EBUSY;
		} else {
			status = 0;
			canIsOpen = true;
		}
		break;
	case MINOR_PPS :
		if (ppsIsOpen > 10) {
			status = -EBUSY;
		} else {
			status = 0;
			++ppsIsOpen;
		}
		break;
	default :
		break;
	}

	return status;
}

static int cf8ch_close(struct inode *inode, struct file *filp)
{
	int i;
	unsigned int minor = iminor(inode);

	switch (minor) {
		case MINOR_TRIG :
		if (trigIsRunning == true) {
			cf8ch_stop_trig();
			trigIsRunning = false;
		}
		trigIsOpen = false;
		if( dataTrigToSendBuff != NULL ){
			vfree(dataTrigToSendBuff);
			dataTrigToSendBuff = NULL;
		}
		break;

	case MINOR_CAN :
		if (canIsRunning == true) {
			cf8ch_stop_can();
			canIsRunning = false;
		}
		
		canIsOpen = false;

		for (i=INDEX_DMA_BUFF1 ; i<NB_DMA_BUFF ; i++)
		{
			if (dataDmaToSendBuff[i] != NULL)
			{
				vfree(dataDmaToSendBuff[i]);
				dataDmaToSendBuff[i] = NULL;
			}
		}
		
/*		if( dataDmaToSendBuff_1 != NULL ){
			vfree(dataDmaToSendBuff_1);
			dataDmaToSendBuff_1 = NULL;
		}
		if( dataDmaToSendBuff_2 != NULL ){
			vfree(dataDmaToSendBuff_2);
			dataDmaToSendBuff_2 = NULL;
		}*/
		break;

	case MINOR_PPS :
		--ppsIsOpen;
		break;

	default :
		break;
	}

	return 0;
}

static const struct spi_device_id cf8ch_ids[] = {
	{ .name = "ifsttar-P3-8ch" },
	{},
};
MODULE_DEVICE_TABLE(spi, cf8ch_ids);

static struct file_operations cf8ch_fops =
{
	.owner 					= 	THIS_MODULE,
	.read       			=   cf8ch_read,
	//.write					=   MyDriver_write,
	.open					=   cf8ch_open,
	.release				=   cf8ch_close,
	.unlocked_ioctl 		= 	cf8ch_ioctl,
};

static const struct of_device_id cf8ch_of_match[] = {
    { .compatible   = "ifsttar,ifsttar-P3-8ch",
	  .data = "hellow world"
	},
    {}
};
MODULE_DEVICE_TABLE(of, cf8ch_of_match);

static int basicProcShow(struct seq_file *m, void *v){
	const struct of_device_id *match;
	match = of_match_device( cf8ch_of_match, &spiHandler->dev);
	seq_printf(m, "data = %s\n",(char *)match->data);
	return 0;
}

static int basicProcOpen(struct inode *inode, struct file *file)
{
	return single_open(file, basicProcShow, NULL);
}

static const struct proc_ops basic_proc_ops =
{
	.proc_open = basicProcOpen,
	.proc_read = seq_read,

};

static int cf8ch_initProc(void){
	basicProcfsEntry = proc_create(DRV_NAME,0666,NULL,&basic_proc_ops);
	if(!basicProcfsEntry){
		return -1;
	}else{
		return 0;
	}
}
static void cf8ch_removeProc(void){
	remove_proc_entry(DRV_NAME, NULL);
}

static int cf8ch_initGpio(void){
	int rc;
	struct device_node *np;
	np = spiHandler->dev.of_node;
	rc = of_property_read_u32(np, "prog-done", &prog_done_level);
	if(0 > rc){
		pr_err("rc = %d   prog_done_level = %d\n", rc, prog_done_level);
		return -1;
	}

	rc = of_property_read_u32(np, "fpga-reset", &fpga_reset_level);
	if(0 > rc){
		pr_err("rc = %d   fpga_reset_level = %d\n", rc, fpga_reset_level);
		return -1;
	}

	rc = of_property_read_u32(np, "fpga-running", &fpga_running_level);
	if(0 > rc){
		pr_err("rc = %d   fpga_running_level = %d\n", rc, fpga_running_level);
		return -1;
	}

	rc = of_property_read_u32(np, "app-running", &app_running_level);
	if(0 > rc){
		pr_err("rc = %d   app_running_level = %d\n", rc, app_running_level);
		return -1;
	}
	
	rc = of_property_read_u32(np, "app-reset", &app_reset_level);
	if(0 > rc){
		pr_err("rc = %d   app_reset_level = %d\n", rc, app_reset_level);
		return -1;
	}
	
	//récupéré prog-done-gpios dans device tree
	prog_done_gpios = of_get_named_gpio(np, "prog-done-gpios", 0);
	if (prog_done_gpios >= 0) {
		devm_gpio_request_one(&spiHandler->dev, prog_done_gpios, GPIOF_IN, "prog-done");
	}else{
		printk("error of_get_named_gpio prog-done-gpios\n");
		return -1;
	}
	
	//récupéré hard-reset-fpga-gpios dans device tree
	hard_reset_fpga_gpios = of_get_named_gpio(np, "hard-reset-fpga-gpios", 0);
	if (hard_reset_fpga_gpios >= 0) {
		devm_gpio_request_one(&spiHandler->dev, hard_reset_fpga_gpios, GPIOF_OUT_INIT_LOW, "hard-reset-fpga");
	}else{
		printk("error of_get_named_gpio hard-reset-fpga-gpios\n");
		return -1;
	}
	
	//récupéré app-reset-fpga-gpios dans device tree
	app_reset_fpga_gpios = of_get_named_gpio(np, "app-reset-fpga-gpios", 0);
	if (app_reset_fpga_gpios >= 0) {
		devm_gpio_request_one(&spiHandler->dev, app_reset_fpga_gpios, GPIOF_OUT_INIT_LOW, "app-reset-fpga");
	}else{
		printk("error of_get_named_gpio prog-done-gpios\n");
		return -1;
	}
	
	gpio_set_value(hard_reset_fpga_gpios,fpga_running_level);
	
	gpio_set_value(app_reset_fpga_gpios,app_reset_level);
	
	gpio_set_value(app_reset_fpga_gpios,app_running_level);
	return 0;
}
static void cf8ch_removeGpio(void){
}

static int cf8ch_initUserSpace(void){
	int rc;
	void *ptr;
	rc = alloc_chrdev_region(&m_oDev, 0, 3, DRV_NAME);//AJOUT 2
	if (rc < 0) {
		pr_err("failed to allocate char device region\n");
		goto fail_chrdev;
	}
	
	cf8chMajor = MAJOR(m_oDev);
	
	m_oClass = class_create(THIS_MODULE, DRV_NAME);
	if (IS_ERR(m_oClass)) {
		pr_err("failed to allocate class\n");
		goto fail_class;
	}
	cdev_init(&m_oCDev, &cf8ch_fops);
	m_oCDev.owner = THIS_MODULE;

	rc = cdev_add(&m_oCDev, m_oDev, 3);//AJOUT 2
	if (rc != 0) {
		pr_err("failed to add char device %d\n", rc);
		goto fail_cdev;
	}
	ptr = device_create (m_oClass, NULL, MKDEV (cf8chMajor, MINOR_TRIG), NULL, DRV_NAME_TRIG);
	if (ptr == NULL) {
		pr_err("failed to create device\n");
		goto fail_device;
	}

	ptr = device_create(m_oClass, NULL, MKDEV (cf8chMajor, MINOR_CAN), NULL, DRV_NAME_CAN);
	if (ptr == NULL) {
		pr_err("failed to create device\n");
		goto fail_device;
	}

	ptr = device_create(m_oClass, NULL, MKDEV (cf8chMajor, MINOR_PPS), NULL, DRV_NAME_PPS);
	if (ptr == NULL) {
		pr_err("failed to create device\n");
		goto fail_device;
	}

	pr_debug("userspace_init succes\n");
	return 0;

fail_device:
	cdev_del(&m_oCDev);

fail_cdev:
	class_destroy(m_oClass);

fail_class:
	unregister_chrdev_region(m_oDev, 1);

fail_chrdev:
	return -1;
}
static void cf8ch_removeUserSpace(void){
	// Delete its node and its entrie in "/dev"
	device_destroy (m_oClass, MKDEV (cf8chMajor, MINOR_TRIG));//trig
	device_destroy (m_oClass, MKDEV (cf8chMajor,MINOR_CAN));//CAN
	device_destroy (m_oClass, MKDEV (cf8chMajor,MINOR_PPS));//PPS
	//    device_destroy(m_oClass, m_oDev);
	cdev_del(&m_oCDev);
	class_destroy(m_oClass);
	unregister_chrdev_region(m_oDev, 3);
}

static int cf8ch_initIrq(void){
	int r ;
	r = request_threaded_irq(spiHandler->irq, NULL,cf8ch_irq_fpga,IRQF_TRIGGER_RISING | IRQF_ONESHOT,"driver name", NULL);
	if (r < 0){
		printk("Unable to register IRQ handler\n");
		return -1;
	}
	return 0;
}
static void cf8ch_removeIrq(void){
	free_irq(spiHandler->irq,NULL);
}

static int cf8ch_checkFPGA(void){
	int i,isBoot;
	u16 version,versionMaj,versionMin;
	
	for(i=0;i<10;i++){
		mdelay(500);
		isBoot = gpio_get_value(prog_done_gpios);
		if(!isBoot){
			printk("value boot = %d try = %d\n",isBoot,i);
		}else{
			printk("FPGA prob : value boot = %d try = %d\n",isBoot,i);
			break;
		}
	}
	if(!isBoot){
		pr_err("%s : prog done to 0\n",DRV_NAME);
		//return -1;
	}
	version = cf8ch_read_register(REGISTER_FPGA_VERSION);
	if (version == 0xFFFF) {
		pr_err("%s : failed read fpga version\n",DRV_NAME);
		return -1;
	} else {
		versionMin = version & 0x00FF;
		versionMaj = version >> 8;
		pr_notice("%s : FPGA version : %02u.%02u\n", DRV_NAME,versionMaj,versionMin);
		if(!(versionMaj > VERSION_MAJ_AUTORISE || (versionMaj == VERSION_MAJ_AUTORISE && versionMin >= VERSION_MIN_AUTORISE))){
			pr_err("%s : FPGA version %02u.%02u < %02u.%02u you need to upload FPGA version\n",DRV_NAME,versionMaj,versionMin,VERSION_MAJ_AUTORISE,VERSION_MIN_AUTORISE);
			return -1;
		}
	}
	return 0;
}

static int cf8ch_probe(struct spi_device *spi){
	spiHandler = spi;
	return 0;
}

static void  cf8ch_remove(void){
}

static struct spi_driver cf8ch_module = {
    .driver = {
        .name   		= DRV_NAME,
        .owner  		= THIS_MODULE,
        .of_match_table = cf8ch_of_match,
    },
    .probe  = cf8ch_probe,
    .remove = (void * )cf8ch_remove,
	.id_table =	cf8ch_ids,
};

static
int
__init cf8ch_init (void)
{
	int i, status;

	printk("start init\n");
	status = spi_register_driver(&cf8ch_module);
	if(status < 0){
		pr_err("status = spi_register_driver(&can8ch_spi_driver) = %d\n", status);
		return -1;
	}
	
	if(spiHandler == NULL){
		pr_err("%s : spiHandler == NULL\n",DRV_NAME);
		return -1;
	}
	
	if(0 > cf8ch_initProc()){
		pr_err("%s : failed to initProc\n",DRV_NAME);
		goto fail_proc;
	}
	
	if(0 > cf8ch_initGpio()){
		pr_err("%s : failed to initGpio\n",DRV_NAME);
		goto fail_gpio;
	}
	
	if(0 > cf8ch_initUserSpace()){
		pr_err("%s : failed to initUserSpace\n",DRV_NAME);
		goto fail_user_space;
	}
	
	if(0 > cf8ch_initIrq()){
		pr_err("%s : failed to initIrq\n",DRV_NAME);
		goto fail_irq;
	}
	
	if(0 > cf8ch_checkFPGA()){
		pr_err("%s : failed to checkFPGA\n",DRV_NAME);
		goto fail_check;
	}

	for (i=INDEX_DMA_BUFF1 ; i<NB_DMA_BUFF ; i++)
	{
		mutex_init(&mutexDmaBuff[i]);
	}
	
	thread_read_running = true;
	taskReadFpga = kthread_create(&cf8ch_thread_read, NULL, "thread read");
	wake_up_process(taskReadFpga);
	
	return 0;
	
fail_check:
	cf8ch_removeIrq();
	
fail_irq:
	cf8ch_removeUserSpace();
	
fail_user_space:
	cf8ch_removeGpio();
	
fail_gpio:
	cf8ch_removeProc();
	
fail_proc:
	spi_unregister_driver(&cf8ch_module);
	return -1;
}

static
void
__exit cf8ch_exit(void){
	cf8ch_removeIrq();
	cf8ch_removeUserSpace();
	cf8ch_removeProc();
	cf8ch_removeGpio();
	
	thread_read_running = false;
	kthread_stop(taskReadFpga);
	
	spi_unregister_driver(&cf8ch_module);
}
module_init (cf8ch_init);
module_exit (cf8ch_exit);


