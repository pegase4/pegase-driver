#ifndef _IFSTTAR_G8CH_
#define _IFSTTAR_G8CH_

#include <linux/types.h>


// Registres de gestion du CAN ADS1278 (Entrées analogiques)
#define REGISTER_STATUS             			0x00				// Registre 16bits (0x00) : Etat de la machine d'état de gestion du CAN ADS1278
#define REGISTER_CONTROL						0x01				// Registre 16bits (0x01) : Registre de control du CAN : '0' => STOP, '1' => START
#define CONTROL_BIT_ACQUISITION     			0x01				// Commande d'activation de l'acquisition analogique
//#define REGISTER_CAN_FE_DIVISOR				     0x02           // Registre 32bits (0x02 (LSB) à 0x03 (MSB)) : plus utilisé, remplacé par REGISTER_FE
#define REGISTER_CAN_FE						0x02				// Registre 32bits (0x02 (LSB) à 0x03 (MSB)) : Fréquence d'Echantillonnage (Hz)
#define REGISTER_PLL_CAN						0x04				// Registre 32bits (0x04 (LSB) à 0x05 (MSB)) : Fréquence de la PLL utilisée pour la gestion du CAN ADS1278 (Hz)
// Paramètre de configuration de l'horloge CAN_CLK générée (multiplication) à partir de l'horloge fournit par la puce GPS (TP2 : TimePulse2). Ceci afin de gérer le gite sur signal TP2
#define REGISTER_CAN_CLK_PARAM_A   			0x08				// Registre 16bits (0x08) : Définit le nombre de période de l'horloge CAN_CLK sur la portion A à partir de l'horloge de référence TP2
#define REGISTER_CAN_CLK_PARAM_B   			0x09				// Registre 16bits (0x09) : Définit le nombre de période de l'horloge CAN_CLK sur la portion B à partir de l'horloge de référence TP2
#define REGISTER_CAN_CLK_PARAM_NB_CYC_A_HI		0x0A				// Registre 16bits (0x0A) : Définit le nombre de période de référence (CAN_PLL) afin de définir le niveau haut CAN_CLK sur la portion A
#define REGISTER_CAN_CLK_PARAM_NB_CYC_A_LOW		0x0B				// Registre 16bits (0x0B) : Définit le nombre de période de référence (CAN_PLL) afin de définir le niveau bas CAN_CLK sur la portion A
#define REGISTER_CAN_CLK_PARAM_NB_CYC_B_HI		0x0C				// Registre 16bits (0x0C) : Définit le nombre de période de référence (CAN_PLL) afin de définir le niveau haut CAN_CLK sur la portion B
#define REGISTER_CAN_CLK_PARAM_NB_CYC_B_LOW		0x0D				// Registre 16bits (0x0D) : Définit le nombre de période de référence (CAN_PLL) afin de définir le niveau bas CAN_CLK sur la portion B

#define REGISTER_CHANNELS_CAN_ACTIVE			0x0E				// Registre 16bits (0x0E) : Indique les entrées analogiques actives. Les 8 bits de poids fort ne sont pas utilisés
#define REGISTER_CAN_MODE       				0x0F				// Registre 16bits (0x0F) : Mode de fonctionnement du CAN ADS1278 : MODE_HIGH_RESOLUTION, MODE_HIGH_SPEED, MODE_LOW_POWER, MODE_LOW_SPEED

//#define REGISTER_DMA_DONE					     0x25           // Adresse du registre modifiée : 0x25 => 0x24
#define REGISTER_DMA_DONE 					0x24                // Registre 16bits (0x24) : 

#define REGISTER_CAN_FREQUENCY_PROCESS 			0x5A                // Registre 32bits (0x5A à 0x5B) : Fréquence de l'horloge utilisée pour la gestion de la machine d'état du CAN au sein du FPGA

#define REGISTER_DATE_CAN					0x61                // Registre 96bits (0x61 à 0x66) : date de l'acquisition analogique : 1er échantillon
#define REGISTER_BURST_CAN					0x6F				// Registre 16bits (0x6F) : Commande pour la lecture des données d'acquisition analogique


// Registres de gestion des TRIG (entrées numériques)
#define REGISTER_TRIG_CONTROL					0x11                // Registre 16bits (0x11) : 
#define REGISTER_CAPTURE_FREQUENCY_PROCESS		0x12                // Registre 16bits (0x12) : 
#define REGISTER_FIFO_COUNT					0x1F                // Registre 16bits (0x1F) : 

#define REGISTER_NB_DATA_DMA_RETURN			0x20                // Registre 32bits (0x20 à 0X21) : 
#define REGISTER_SET_NB_TRIG_OFF				0x22                // Registre 16bits (0x22) : 
#define REGISTER_STATUS_TRIG_OFF				0x23                // Registre 16bits (0x23) : 
//#define REGISTER_TIMEOUT_TRIG				     0x27           // Adresse du registre modifié : 0x27 => 0x26
#define REGISTER_TIMEOUT_TRIG					0x26                // Registre 16bits (0x26) : 

#define REGISTER_CAPTURE_INITIAL_SECONDS_GET		0x51                // Registre 64bits (0x51 à 0x54) : 

#define REGISTER_TYPE_IRQ					0x5D                // Registre 16bits (0x5D) : 

#define REGISTER_CHANNELS_TRIG_ACTIVE			0x60				// Registre 16bits (0x60) : Indique les entrées numériques actives. Les 8 bits de poids fort ne sont pas utilisés
#define REGISTER_TRIG_OFF_RST					0x6A				// Registre 16bits (0x6A) : 


// Registres fonction GPS
#define REGISTER_STATUS_GPS                       0x27                // Registre 16bits (0x27) : GPS status : 0 = Non valide, 1 = Fix GPS, 2 = Fix DGPS, ...
#define REGISTER_FREQUENCE_TP2                    0x28                // Registre 32bits (0x28 (LSB) à 0x29 (MSB)) : Fréquence (Hz) du signal TimePulse2 (TP2) fournit par la puce GPS NEO-M8T
//#define REGISTER_QERR					          0x28           // Adresse du registre modifiée : 0x28 => 0x2A
#define REGISTER_QERR                             0x2A                // Registre 32bits (0x2A (LSB) à 0x2B (MSB)) : Valeur qErr fournit par la puce GPS
//#define REGISTER_TIME_EPOCH				          0x2A           // Adresse du registre modifiée : 0x2A => 0x2C
#define REGISTER_TIME_EPOCH                       0x2C                // Registre 64bits (0x2C (LSB) à 0x2F (MSB)) : Date fournit par la puce GPS au FPGA


// Registres FIFO data
#define REGISTER_DATA_TRIG				     0x30                // Registre 112bits (0x30 à 0x36) : 
#define REGISTER_IRQ_TRIG_DONE			     0x3A                // Registre 16bits (0x3A) : 
#define REGISTER_BURST_TRIG				     0x41				// Registre 16bits (0x41) : Commande pour la lecture des données d'acquisition numérique


// Registres version FPGA
#define REGISTER_FPGA_VERSION					0x50                // Registre 16bits (0x50) : Version du FPGA : FW_MAJOR (8bits de poids fort). FW_MINOR (8bits de poids faible)


#define CF8CH_MAGIC							'A'	
#define CF8CH_ENABLE_CAN_ACQUISITION			_IO (CF8CH_MAGIC, 0x01)
#define CF8CH_DISABLE_CAN_ACQUISITION			_IO (CF8CH_MAGIC, 0x02)
//#define CF8CH_SET_CAN_FE_DIVISOR				_IOW(CF8CH_MAGIC, 0x03, int)
//#define CF8CH_GET_CAN_FE_DIVISOR				_IOR(CF8CH_MAGIC, 0x04, int)
#define CF8CH_SET_CAN_FE           			_IOW(CF8CH_MAGIC, 0x03, int)
#define CF8CH_GET_CAN_FE      				_IOR(CF8CH_MAGIC, 0x04, int)
#define CF8CH_GET_PLL_CAN_CLOCK				_IOR(CF8CH_MAGIC, 0x05, int)
#define CF8CH_GET_NB_DATA_DMA					_IOR(CF8CH_MAGIC, 0x08, int)
#define CF8CH_SET_NB_DATA_DMA					_IOW(CF8CH_MAGIC, 0x09, int)
#define CF8CH_SET_CAN_CHANNELS				_IOW(CF8CH_MAGIC, 0x07, unsigned char)
#define CF8CH_SET_CHANELS_TRIG				_IOW(CF8CH_MAGIC, 0x0B, int)
#define CF8CH_SET_TIMEOUT_TRIG				_IOW(CF8CH_MAGIC, 0x0D, int)
#define CF8CH_GET_TIMEOUT_TRIG				_IOW(CF8CH_MAGIC, 0x0E, int)
#define CF8CH_SET_CAN_MODE                        _IOW(CF8CH_MAGIC, 0x10, int)
#define CF8CH_GET_CAN_MODE                        _IOR(CF8CH_MAGIC, 0x11, int)
#define CF8CH_SET_FREQUENCE_TP2                   _IOW(CF8CH_MAGIC, 0x12, int)
#define CF8CH_GET_FREQUENCE_TP2                   _IOR(CF8CH_MAGIC, 0x13, int)
#define CF8CH_SET_CAN_CLK_PARAM_A                 _IOW(CF8CH_MAGIC, 0x14, int)
#define CF8CH_GET_CAN_CLK_PARAM_A                 _IOR(CF8CH_MAGIC, 0x15, int)
#define CF8CH_SET_CAN_CLK_PARAM_B                 _IOW(CF8CH_MAGIC, 0x16, int)
#define CF8CH_GET_CAN_CLK_PARAM_B                 _IOR(CF8CH_MAGIC, 0x17, int)
#define CF8CH_SET_CAN_CLK_PARAM_NB_CYCLE_A_HI     _IOW(CF8CH_MAGIC, 0x18, int)
#define CF8CH_GET_CAN_CLK_PARAM_NB_CYCLE_A_HI     _IOR(CF8CH_MAGIC, 0x19, int)
#define CF8CH_SET_CAN_CLK_PARAM_NB_CYCLE_A_LOW    _IOW(CF8CH_MAGIC, 0x1A, int)
#define CF8CH_GET_CAN_CLK_PARAM_NB_CYCLE_A_LOW    _IOR(CF8CH_MAGIC, 0x1B, int)
#define CF8CH_SET_CAN_CLK_PARAM_NB_CYCLE_B_HI     _IOW(CF8CH_MAGIC, 0x1C, int)
#define CF8CH_GET_CAN_CLK_PARAM_NB_CYCLE_B_HI     _IOR(CF8CH_MAGIC, 0x1D, int)
#define CF8CH_SET_CAN_CLK_PARAM_NB_CYCLE_B_LOW    _IOW(CF8CH_MAGIC, 0x1E, int)
#define CF8CH_GET_CAN_CLK_PARAM_NB_CYCLE_B_LOW    _IOR(CF8CH_MAGIC, 0x1F, int)
#define CF8CH_ENABLE_TRIG_ACQUISITION			_IO (CF8CH_MAGIC, 0x51)
#define CF8CH_DISABLE_TRIG_ACQUISITION			_IO (CF8CH_MAGIC, 0x52)
#define CF8CH_GET_STATUS_TRIG_OFF				_IOR(CF8CH_MAGIC, 0x55, int)
#define CF8CH_GET_CAN_FREQUENCY_PROCESS           _IOR(CF8CH_MAGIC, 0x57, int)
#define CF8CH_GET_CAN_CHANNELS				_IOR(CF8CH_MAGIC, 0x58, unsigned char)
#define CF8CH_GET_CHANELS_TRIG				_IOR(CF8CH_MAGIC, 0x59, int)
#define CF8CH_GET_NB_ERR_OF					_IOR(CF8CH_MAGIC, 0x5A, uint32_t)
#define CF8CH_RESET_TRIG_OFF					_IO (CF8CH_MAGIC, 0x5B)
#define CF8CH_GET_FPGA_VERSION				_IOR(CF8CH_MAGIC, 0x5C, uint32_t)
#define CF8CH_SET_NB_TRIG_OFF					_IOW(CF8CH_MAGIC, 0x80, int)
#define CF8CH_GET_NB_TRIG_OFF					_IOR(CF8CH_MAGIC, 0x81, int)
#define CF8CH_GET_TIME_ACQUISITION_TS			_IOR(CF8CH_MAGIC, 0x82, long long int)
#define CF8CH_GET_TIME_ACQUISITION_MICRO		_IOR(CF8CH_MAGIC, 0x83, int)
	


#define NB_DMA_BUFF                                 2               // NB_DMA_BUFF Max = 16
#define INDEX_DMA_BUFF1                             0


typedef struct _Date_S_NS Date_S_NS_t;
struct _Date_S_NS {
     uint64_t ts;
     uint32_t nanoS;
} __attribute__ ((packed));

struct ppsInfos {
    uint64_t time;          /**< \brief Unix timestamp of the described informations */
    uint32_t period;        /**< \brief Size of the period in timer step */
    int32_t qerr_beginning; /**< \brief Offset of the PPS in the beginning of the second */
    int32_t qerr_end;       /**< \brief Offset of the PPS in the end of the second */
};

typedef struct _ReadTrigChanel ReadTrigChanel_t;
struct _ReadTrigChanel {
     uint16_t trig;
     uint64_t ts;
     uint32_t nanoS; 
} __attribute__ ((packed));

typedef struct _ReadTrigChanel48 ReadTrigChanel48_t;
struct _ReadTrigChanel48 {
	unsigned int trig  : 8;
	unsigned int dtS   : 12;
	unsigned int nanoS : 28;
} __attribute__ ((packed));

typedef struct _ReadTrigChanelCompressed ReadTrigChanelCompressed_t;
struct _ReadTrigChanelCompressed {
	ReadTrigChanel_t firstTrig;
	ReadTrigChanel48_t* otherTrigs;
} __attribute__ ((packed));
#endif /* _IFSTTAR_G8CH_ */ 
