#!/bin/sh -e

echo "Updating FPGA firmware of 8ch daughter board"

# Arret de Zeus-apps
if [ -z $1 ]; then
  echo "Ce script flash le firmware du FPGA mais pour cela \"Zeus-apps\" doit etre arrete. \"Zeus-apps\" sera relance a la suite du flashage. Voulez vous realiser le flashage du firmware du FPGA (\"Yes\" or \"No\") ?"
  read ValidationDump
else
  echo "Ce script va arreter \"Zeus-apps\", realiser le flashage du firmware du FPGA puis \"Zeus-apps\" sera relance a la suite du flashage."
  ValidationDump=$1
fi

case $ValidationDump in
    Yes )	# Arret de zeus-apps
        if [ "S$(pidof zeus-apps)" != "S" ]; then
            echo "Arret de \"Zeus-apps\""
            systemctl stop zeus-apps
            sleep 5
        else
            echo "\"Zeus-apps\" n'est pas cours d'execution"
        fi

        # Dechargement du driver ifsttar_g8ch, s'il est encore present apres l'arret de Zeus-apps (bugg ?)
        if [ "S$(lsmod | grep ifsttar_g8ch)" != "S" ]; then
            echo "Le driver \"ifsttar_g8ch\" n'a pas ete decharge : Dechargement de \"ifsttar_g8ch\""
            rmmod ifsttar_g8ch
            sleep 2
        fi

        # Definition de la version pour le dump de la flash du FPGA, soit via un argument, soit via un requete du script a l'utilisateur
        CHEMIN_ARCHIVE_FIRMWARE="/lib/firmware/ifsttar"
        cd $CHEMIN_ARCHIVE_FIRMWARE

        if test $# -eq 2; then
            VERSION=$2
            echo "La version du firmware utilisee pour flasher le FPGA : v$VERSION"
        else
            echo "Quelle est la version du firmware a flasher pour le FPGA (indiquer uniquement le numéro de version (ex : 02.03)) : "
            echo "Voici la liste des firmware disponible pour l'update:"
            find *.gz
            read VERSION
        fi

        NOM_FICHIER_ARCHIVE_FIRMWARE="fwg8ch_$VERSION.bin"
        NOM_FICHIER_ARCHIVE_FIRMWARE_COMPRESSE="fwg8ch_$VERSION.bin.gz"
        NOM_FICHIER_ARCHIVE_FIRMWARE_MD5="fwg8ch_$VERSION.bin.md5"

        modprobe spidev

        # Extract firmware
        zcat "$CHEMIN_ARCHIVE_FIRMWARE/$NOM_FICHIER_ARCHIVE_FIRMWARE_COMPRESSE" > "/tmp/$NOM_FICHIER_ARCHIVE_FIRMWARE"

	# Vérification du MD5        
        cp "$CHEMIN_ARCHIVE_FIRMWARE/$NOM_FICHIER_ARCHIVE_FIRMWARE_MD5" "/tmp/$NOM_FICHIER_ARCHIVE_FIRMWARE_MD5"
	PWD=pwd
	cd /tmp
	echo "md5sum --check $NOM_FICHIER_ARCHIVE_FIRMWARE_MD5"
        md5sum --check "$NOM_FICHIER_ARCHIVE_FIRMWARE_MD5"
        cd $PWD

        # Set FPGA in programming mode
        echo 73 > /sys/class/gpio/export
        echo out > /sys/class/gpio/gpio73/direction
        echo 0 > /sys/class/gpio/gpio73/value
        devmem2 0x480020E2 h 0x10C

        # Write firmware in the FPGA
        flashrom -p linux_spi:dev=/dev/spidev0.1,spispeed=1024 -w "/tmp/$NOM_FICHIER_ARCHIVE_FIRMWARE"

        # Set FPGA in execution mode
        echo 1 > /sys/class/gpio/gpio73/value
        echo 73 > /sys/class/gpio/unexport

        # Cleanup
        rm "/tmp/$NOM_FICHIER_ARCHIVE_FIRMWARE"
        rm "/tmp/$NOM_FICHIER_ARCHIVE_FIRMWARE_MD5"

        rmmod spidev
        echo "Update FPGA done"

        # redemarrage de Zeus-apps
		echo "Redemarage de \"Zeus-apps\""
		systemctl start zeus-apps
		exit 0;;


    No )	echo "Commande flashages annule"
		exit 0;;


    * ) 	echo "Choix non disponible"
		exit 1;;
esac
