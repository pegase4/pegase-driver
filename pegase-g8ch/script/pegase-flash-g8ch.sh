#!/bin/sh -e

echo "Updating FPGA firmware of 8ch daughter board"

VERSION=02.01
if test $# -eq 1; then
    VERSION=$1
fi
echo "Firmware version is $VERSION"

modprobe spidev

# Extract firmware
cd /lib/firmware/pegase/
zcat /lib/firmware/pegase/pegase-g8ch/fwg8ch_$VERSION.bin.gz > /tmp/fwg8ch_$VERSION.bin
cp /lib/firmware/pegase/pegase-g8ch/fwg8ch_$VERSION.bin.md5 /tmp/
cd /tmp/
md5sum -s -c fwg8ch_$VERSION.bin.md5

# Set FPGA in programming mode
echo 62 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio62/direction
echo 0 > /sys/class/gpio/gpio62/value

# Write firmware in the FPGA
flashrom -p linux_spi:dev=/dev/spidev0.1,spispeed=1024 -w /tmp/fwg8ch_$VERSION.bin

# Set FPGA in execution mode
echo 1 > /sys/class/gpio/gpio62/value
echo 62 > /sys/class/gpio/unexport

# Cleanup
rm /tmp/fwg8ch_$VERSION.bin

rmmod spidev
echo "Done"

