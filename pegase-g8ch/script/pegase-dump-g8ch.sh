#!/bin/sh -e

echo "Dumping FPGA firmware of 8ch daughter board"

VERSION=02.00
DATE=`date '+%Y-%m-%d_%H:%M:%S'`
if test $# -eq 1; then
    VERSION=$1
fi

modprobe spidev

# Extract firmware

# Set FPGA in programming mode
echo 73 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio73/direction
echo 0 > /sys/class/gpio/gpio73/value
devmem2 0x480020E2 h 0x10C

# Write firmware in the FPGA
if [ ! -d "/lcpc/FPGAFirmwareDump" ];then
mkdir /lcpc/FPGAFirmwareDump/
fi
flashrom -p linux_spi:dev=/dev/spidev0.1,spispeed=1024 -r /lcpc/FPGAFirmwareDump/fw8ch_$DATE.bin

# Set FPGA in execution mode
echo 1  /sys/class/gpio/gpio73/value
echo 73 > /sys/class/gpio/unexport

rmmod spidev
echo "Done"

