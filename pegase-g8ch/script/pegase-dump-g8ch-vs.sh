#!/bin/sh -e

echo "Dumping FPGA firmware of 8ch daughter board"

# Information de l'utilisation sur l'arret necessaire de Zeus-apps
if [ -z $1 ]; then
  echo "Ce script realise le dump du firmware du FPGA mais pour cela \"Zeus-apps\" doit etre arrete. \"Zeus-apps\" sera relance a la suite du dump. Voulez vous realiser le dump du firmware du FPGA (\"Yes\" or \"No\") ?"
  read ValidationDump
else
  echo "Ce script va arreter \"Zeus-apps\", realiser le dump du firmware du FPGA puis \"Zeus-apps\" sera relance a la suite du dump."
  ValidationDump=$1
fi

case $ValidationDump in
    Yes )
        # Definition de la version pour le dump de la flash du FPGA, soit via un argument, soit via un requete du script a l'utilisateur
        if test $# -eq 2; then
            VERSION=$2
            echo "La version du firmware retenue pour dumper le FPGA : v$VERSION"
        else
            echo "Quelle est la version du dump de la flash du FPGA : "
            read VERSION
        fi

        # Definition de la date du build pour le dump de la flash du FPGA
        DATE=`date '+%Y-%m-%d_%H:%M:%S'`

    	# Arret de zeus-apps
        if [ "S$(pidof zeus-apps)" != "S" ]; then
            echo "Arret de \"Zeus-apps\""
            systemctl stop zeus-apps
        	sleep 5
        else
            echo "\"Zeus-apps\" n'est pas cours d'execution"
        fi

        # Dechargement du driver ifsttar_g8ch, s'il est encore present apres l'arret de Zeus-apps (bugg ?)
        if [ "S$(lsmod | grep ifsttar_g8ch)" != "S" ]; then
            echo "Le driver \"ifsttar_g8ch\" n'a pas ete decharge : Dechargement de \"ifsttar_g8ch\""
            rmmod ifsttar_g8ch
            sleep 2
        fi

        # Definition des chemin et des noms de fichiers
        CHEMIN_CREATION_FIRMWARE="/lcpc/FPGAFirmwareDump"
        NOM_FICHIER_CREATION_FIRMWARE="fgw8ch_$VERSION-build$DATE.bin"

        CHEMIN_ARCHIVE_FIRMWARE="/lib/firmware/ifsttar"
        NOM_FICHIER_ARCHIVE_FIRMWARE="fwg8ch_$VERSION.bin"
        NOM_FICHIER_ARCHIVE_FIRMWARE_COMPRESSE="fwg8ch_$VERSION.bin.gz"
        NOM_FICHIER_ARCHIVE_FIRMWARE_MD5="fwg8ch_$VERSION.bin.md5"

        # Extract firmware
        ####################

        # Chargement du module "spidev"
        modprobe spidev

        # Set FPGA in programming mode
        echo 73 > /sys/class/gpio/export
        echo out > /sys/class/gpio/gpio73/direction
        echo 0 > /sys/class/gpio/gpio73/value
        devmem2 0x480020E2 h 0x10C

        # Read firmware in the FPGA
        if [ ! -d "$CHEMIN_CREATION_FIRMWARE" ];then
        mkdir $CHEMIN_CREATION_FIRMWARE
        fi
        flashrom -p linux_spi:dev=/dev/spidev0.1,spispeed=1024 -r "$CHEMIN_CREATION_FIRMWARE/$NOM_FICHIER_CREATION_FIRMWARE"
        echo "Le dump de la flash du FPGA a ete copie ici : $CHEMIN_CREATION_FIRMWARE/$NOM_FICHIER_CREATION_FIRMWARE"

        # Set FPGA in execution mode
        echo 1 > /sys/class/gpio/gpio73/value
        echo 73 > /sys/class/gpio/unexport

        rmmod spidev
        echo "Dump FPGA done"

        echo "Voulez vous compressez (+MD5) et archivez le firmware du FPGA (\"Yes\" or \"No\")"
        read ValidationArchive

        case $ValidationArchive in
            Yes )   # Deplacement des fichiers dans le dossier d'archive
                    mv "$CHEMIN_CREATION_FIRMWARE/$NOM_FICHIER_CREATION_FIRMWARE" "$CHEMIN_ARCHIVE_FIRMWARE/$NOM_FICHIER_ARCHIVE_FIRMWARE"
            
            	     # Generation du fichier MD5
                    PWD=pwd
                    cd $CHEMIN_ARCHIVE_FIRMWARE
                    echo "md5sum $NOM_FICHIER_ARCHIVE_FIRMWARE > $NOM_FICHIER_ARCHIVE_FIRMWARE_MD5"
                    md5sum "$NOM_FICHIER_ARCHIVE_FIRMWARE" > "$NOM_FICHIER_ARCHIVE_FIRMWARE_MD5"
                    cd $PWD
                    echo "Creation du fichier $CHEMIN_ARCHIVE_FIRMWARE/$NOM_FICHIER_ARCHIVE_FIRMWARE_MD5"

            	     # Compression du firmware
                    gzip -c "$CHEMIN_ARCHIVE_FIRMWARE/$NOM_FICHIER_ARCHIVE_FIRMWARE" > "$CHEMIN_ARCHIVE_FIRMWARE/$NOM_FICHIER_ARCHIVE_FIRMWARE_COMPRESSE"
                    rm "$CHEMIN_ARCHIVE_FIRMWARE/$NOM_FICHIER_ARCHIVE_FIRMWARE"
                    echo "Compression du fichier $CHEMIN_CREATION_FIRMWARE/$NOM_FICHIER_CREATION_FIRMWARE dans $CHEMIN_ARCHIVE_FIRMWARE/$NOM_FICHIER_ARCHIVE_FIRMWARE_COMPRESSE"

                    # Copie dans le dossier /tmp pour recuperation par scp
                    cp "$CHEMIN_ARCHIVE_FIRMWARE/$NOM_FICHIER_ARCHIVE_FIRMWARE_COMPRESSE" /tmp
                    cp "$CHEMIN_ARCHIVE_FIRMWARE/$NOM_FICHIER_ARCHIVE_FIRMWARE_MD5" /tmp
                    echo "Copie des fichiers $NOM_FICHIER_ARCHIVE_FIRMWARE_COMPRESSE et $NOM_FICHIER_ARCHIVE_FIRMWARE_MD5 pour recuperation par scp dans le dossier /tmp"

            		echo "Archive done"

                    echo "Redemarrage de \"Zeus-apps\""
            		systemctl start zeus-apps
            		exit 0;;

            No )
                    echo "No archive done"

                    echo "Redemarrage de \"Zeus-apps\""
                    systemctl start zeus-apps
                    exit 0;;


            * )
                    echo "Choix non disponible"

                    echo "Redemarrage de \"Zeus-apps\""
            		systemctl start zeus-apps
            		exit 1;;
        esac

        exit 0;;

    No )	echo "Commande dump annule"
		exit 0;;


    * ) 	echo "Choix non disponible"
		exit 1;;
esac
