#include <linux/proc_fs.h>
#include <linux/cdev.h>
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/uaccess.h>
#include <linux/of_platform.h>
#include <linux/of_gpio.h>
#include <linux/spi/spi.h>
#include <linux/of_irq.h>
#include <linux/poll.h>

#include "ifsttar_hf8ch.h"

MODULE_AUTHOR ("Arthur BOUCHE <arthur.bouche@ifsttar.fr>");
MODULE_DESCRIPTION ("IFSTTAR hf 8ch Driver");
MODULE_LICENSE ("GPL");
MODULE_VERSION ("0.1");


static int VERBOSEPERIOD = 0;
module_param(VERBOSEPERIOD, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(VERBOSEPERIOD, "VERBOSEPERIOD");

#define DRV_NAME    	"ifsttar_hf8ch"

#define VERSION_MAJ_AUTORISE    	1
#define VERSION_MIN_AUTORISE    	0

#define MINOR_PPS 0
#define MINOR_CAN_DAC 1
#define MINOR_REGISTRE 2

#define NB_FILE_DESC 5
#define MAX_DATA_READ_KERNEL 250000


int isOpen[NB_FILE_DESC] = {0,0,0,0,0};

struct spi_device *spiHandler = NULL;

int prog_done_level,fpga_reset_level,fpga_running_level;
int prog_done_gpios, hard_reset_fpga_gpios;

static bool thread_read_running = false;

static dev_t     m_oDev;
unsigned int   	 m_hf8chMajor = 0;
static struct    cdev m_oCDev;
static struct    class *m_oClass = NULL;
unsigned int     count = 0;

uint8_t nb_chanel;
// this spinlock is used for disabling interrupts while spi_sync() is running

static struct proc_dir_entry *basicProcfsEntry;


static DECLARE_WAIT_QUEUE_HEAD(ReadWait);

static DEFINE_MUTEX(hf8ch_lock);


static struct task_struct *task_read_it;




static u16 type_irq = 0;
/*********PPS***********
 * *********************/
static ppsInfos_t ppsInfoFromFpga;
static bool flag_pps = false;
static DECLARE_WAIT_QUEUE_HEAD(ReadPPS);
static DEFINE_MUTEX(mutexPps);

/*********CAN***********
 * *********************/
bool flag_dma = 0;
int nbEchantillion = 0;
int nbChannel = 0;
static DECLARE_WAIT_QUEUE_HEAD(ReadData);
static DEFINE_MUTEX(mutexDmaBuff);

bool flag_read = 0;
static DECLARE_WAIT_QUEUE_HEAD(read);

ConfhfCan_t ConfhfCan;
/*
 *  CAN Registers definition
 */
#define CMD_READ(x)                 (0x80 | (x & 0x7F))
#define CMD_WRITE(x)                (0x00 | (x & 0x7F))
#define ADDR_RAM_FPGA 				 0x10

#define ADDR_CMD                    	0x00 //déclanche tire en ecrivant 1
#define CONTROL_BIT_TRIG_SOFT	    	0x0001 // 0000 0001
#define CONTROL_BIT_RESET	    		0x0002 // 0000 0010
#define CONTROL_BIT_CONFIG	    		0x0004 // 0000 0100
#define CONTROL_DMA_DONE 	    		0x0008 // 0000 1000
#define CONTROL_BIT_TRIG_DATE	    	0x0010 // 0001 0000
#define CONTROL_BIT_SHOT_ON_PPS	    	0x0020 // 0010 0000
#define CONTROL_BIT_TRIG_EXTERNE    	0x0040 // 0100 0000
#define CONTROL_BIT_TRIG_EXTERNE_RECOPI	0x0080 // 1000 0000
#define CONTROL_BIT_ENABLE_POWER       	0x0100 // 0001 0000 0000
#define CONTROL_BIT_ENABLE_PPS2       	0x0200 // 0010 0000 0000
#define CONTROL_BIT_ENABLE_RAMPE       	0x8000 // 1000 0000 0000 0000

#define ADDR_DIGIT_DELAY         		0x01 // temps entre le tire et la numérisation
#define ADDR_PRF   	 	        		0x02 //temps d'attente entre 2 rite unitaire
#define ADDR_NBR_OF_AVG          		0x03 //nombre tire unitaire
#define ADDR_NBR_TX_SAMPLE       		0x04
#define ADDR_NBR_RX_SAMPLE       		0x05 //nombre d echantillion par voies
#define ADDR_NBR_RX_SAMPLE_BYTES 		0x06 //nombre d'octet (voies*ADDR_NBR_TX_SAMPLE*2)
#define ADDR_GAIN_CH0 					0x07 //gain voie 0
#define ADDR_GAIN_CH1            		0x08 //gain voie 1
#define ADDR_GAIN_CH2            		0x09 //gain voie 2
#define ADDR_GAIN_CH3            		0x0A //gain voie 3
#define ADDR_GAIN_CH4            		0x0B //gain voie 4
#define ADDR_GAIN_CH5            		0x0C //gain voie 5
#define ADDR_GAIN_CH6            		0x0D //gain voie 6
#define ADDR_GAIN_CH7            		0x0E //gain voie 7
#define ADDR_TX_ACTIV_CH         		0x0F // un seul à la fois
#define ADDR_BURST_DATA_RX       		0x10 //aller chercher les données et l'entete
#define ADDR_FREQ_SAMPLE_RX				0x11
#define ADDR_SECONDE_UTC_LSB			0x12
#define ADDR_SECONDE_UTC_MSB			0x13
#define ADDR_QERR						0x14
#define ADDR_PLL_CAPTRURE				0x15
#define ADDR_FIRST_CPT_CAPTURE			0x16 
#define ADDR_FIRST_SECONDE_UTC_LSB		0x17 
#define ADDR_FIRST_SECONDE_UTC_MSB		0x18
//#define ADDR_FIRST_CPT_CAPTURE			0x19 
#define ADDR_LAST_SECONDE_UTC_LSB		0x1A 
#define ADDR_LAST_SECONDE_UTC_MSB		0x1B
#define ADDR_DATE_DECLANCHEMENT_LSB		0x1C
#define ADDR_DATE_DECLANCHEMENT_MSB		0x1D
#define ADDR_AMPLITUDE_TX				0x1E
#define ADDR_LED_BLINK					0x22
#define ADDR_SYNC_PPS 					0x23
#define ADDR_DELAIS_ACQ					0x24

#define ADDR_BURST_SIGNAL_TX_DAC_1		0x1F 
#define ADDR_BURST_SIGNAL_TX_DAC_2		0x20 

#define ADDR_CHANNEL_ACTIVATION_SEUIL	0x21 // (9bit) 1 bit d'activation + masq des entré qui regarde 
#define ADDR_SEUIL_POS				0x26 // seuil positif en valeur ADC (sur 16 bit signe)
#define ADDR_SEUIL_NEG				0x27 // seuil negatif en valeur ADC (sur 16 bit signe) 27 doit etre inferireur au 26
#define ADDR_PRE_TRIG				0x28 // pre trig (13 bit) 8191
#define ADDR_MASK_CHANNEL_RX			0x29 // pre trig (13 bit) 8191
/*
 *  CAN Low level access
 */

static
u32
hf8ch_read_register32(int addr)
{
	ssize_t status;
	u32 result;
	u8 cmd = CMD_READ(addr);
	if (spiHandler == NULL) {
		pr_err("spiHandler is null !\n");
		return 0;
	}
	status = spi_write_then_read(spiHandler, &cmd, 1, &result, 4);
	if (status < 0) {
		pr_err("Spi read32 failed @addr = 0x%02X\n", addr);
		return 0;
	}
	result = cpu_to_be32(result);
	return result;
}

static
void
hf8ch_write_register32(int addr, u32 value)
{
	ssize_t status;
	u8 tx[1 + sizeof(value)];

	if (spiHandler == NULL) {
		pr_err("spiHandler is null !\n");
		return;
	}
	tx[0] = CMD_WRITE(addr);
	tx[1] = (value >> 24) & 0xFF;
	tx[2] = (value >> 16) & 0xFF;
	tx[3] = (value >> 8) & 0xFF;
	tx[4] =  value & 0xFF;
	status = spi_write(spiHandler, tx, sizeof(tx));
	if (status < 0) {
		pr_err("Spi write failed @addr = 0x%02X\n", addr);
		return;
	}
}


static
u16
hf8ch_get_type_irq(void){
	__u32 irq = hf8ch_read_register32(ADDR_CMD);
	return irq >> 16;
}

static int flag_it = 0;

/*
 *  GPIO IRQs
 *  - IT data ready, throw when data are available to be read in the FPGA
 *  - IT timing, throw when the CAN sample data inputs
 */
static irqreturn_t hf8ch_irq_fpga (int irq, void *dev_id)
{
	flag_it = 1;
	wake_up_interruptible(&ReadWait);
	return IRQ_HANDLED;
}



static
int
hf8ch_read_fpga_dma(void)
{

	return 0;
}

int hf8ch_read_fpga_pps(void)
{
	static u64 dateSeconde;
	dateSeconde = hf8ch_read_register32(ADDR_SECONDE_UTC_MSB);
	dateSeconde = dateSeconde << 32;
	dateSeconde += hf8ch_read_register32(ADDR_SECONDE_UTC_LSB);
	mutex_lock(&mutexPps);
	ppsInfoFromFpga.time = dateSeconde;
	ppsInfoFromFpga.period = hf8ch_read_register32(ADDR_PLL_CAPTRURE);
    ppsInfoFromFpga.qerr = hf8ch_read_register32(ADDR_QERR);
	mutex_unlock(&mutexPps);

    flag_pps = true;
	wake_up_interruptible (&ReadPPS);
    return 0;
}
static int thread_read_it(void *data)
{
	int ret;
	while (thread_read_running) {
		ret = wait_event_interruptible_timeout(ReadWait, flag_it != 0, HZ * 2);
		flag_it = 0;
		type_irq = hf8ch_get_type_irq();
		//printk("start thread_read_it %d %d\n",type_irq, flag_dma);//TODO
		if(type_irq != 0b1111111111111111){
			if ( (type_irq & 0b0000000000000001) == 0b0000000000000001) {
				printk("start FPGA DMA\n");
				//ret = hf8ch_read_fpga_dma();
				flag_dma = 1;
				wake_up_interruptible (&ReadData);//wakeup pool
				//printk("end FPGA DMA\n");
			}else{
				hf8ch_read_fpga_pps();	
			}
		}
	//printk("end thread_read_it %d %d\n",flag_it, flag_dma);//TODO
	}
	return 0;
}

static int hf8ch_init_irq(void)
{
	int rc;

	printk("irq = %d\n",spiHandler->irq);
	rc = request_threaded_irq(spiHandler->irq, NULL,hf8ch_irq_fpga,IRQF_TRIGGER_RISING | IRQF_ONESHOT,DRV_NAME, NULL);
	if (rc < 0){
		printk("Unable to register IRQ handler\n");
		return -1;
	}
	return 0;
}

static void hf8ch_irq_exit(void)
{
	free_irq(spiHandler->irq,NULL);
}


/*
 *  Driver CHAR Fops
 */
static int hf8ch_open (struct inode *inode, struct file *filp)
{
	int status = 0;
	unsigned int minor = iminor(inode);
	mutex_lock(&hf8ch_lock);
	if (isOpen[minor] == 2){
		status = -EBUSY;
	}else{
		isOpen[minor] = isOpen[minor] + 1;
	}
	mutex_unlock(&hf8ch_lock);
	return status;
}

static int hf8ch_close (struct inode *inode, struct file *filp)
{
	unsigned int minor = iminor(inode);
	mutex_lock(&hf8ch_lock);
	isOpen[minor] = isOpen[minor] -1;
	mutex_unlock(&hf8ch_lock);
	return 0;
}

__u16*data;

static ssize_t hf8ch_read_data(struct file *filp, char *buf, size_t size, loff_t *offp){
	u8 temp;
	int doneCpt = 0;
	__u32 u32_temp;
		//printk("start hf8ch_read_data\n");//TODO

	int dataToRead;
	struct spi_transfer transfer[2] ;
	int ret,i;

	for(doneCpt = 0; doneCpt<10;doneCpt++){
		u32_temp = hf8ch_read_register32(ADDR_CMD);
		if(((u32_temp >> 16) & 0b0000000000000001) == 0b0000000000000001){
			u32_temp |= CONTROL_DMA_DONE;
			hf8ch_write_register32(ADDR_CMD,u32_temp);
			printk("retry dma done %d %d\n",(u32_temp >> 16),doneCpt);//TODO

		}else{
			printk("dma done\n");//TODO
			flag_dma = 0;
			break;
		}
	}

    printk("taile data = %d et taille size = %d\n",sizeof(data),size);
	ConfhfCan.cptfirst = hf8ch_read_register32(ADDR_FIRST_CPT_CAPTURE);
	ConfhfCan.timefirst = hf8ch_read_register32(ADDR_FIRST_SECONDE_UTC_MSB);
	ConfhfCan.timefirst = ConfhfCan.timefirst << 32;
	ConfhfCan.timefirst += hf8ch_read_register32(ADDR_FIRST_SECONDE_UTC_LSB);
	///////////////////////////////// 
	memset(data, 0, size);
	dataToRead = size;
#if 1
	i = 0;
	memset(transfer, 0, sizeof(transfer));
	temp = CMD_READ(ADDR_BURST_DATA_RX);
	while(dataToRead != 0){
		transfer[0].tx_buf = &temp;
		transfer[0].len = sizeof(temp);
		transfer[1].rx_buf = data+(i*HF8CH_MAX_DATA_READ/2);
		transfer[1].len =((dataToRead > HF8CH_MAX_DATA_READ) ? HF8CH_MAX_DATA_READ: dataToRead); /** sizeof(u16)*/ //4095*2*8
		ret = spi_sync_transfer(spiHandler,&transfer[0],2);
		if (ret < 0) {
			pr_err("Spi read Data failed ret = %d\n",ret);
			goto exit;
		}
		printk(" data to read = %d read done = %d\n",dataToRead,transfer[1].len);
		//printk(" data  = %d data = %d\n",data[0+],transfer[1].len);

		dataToRead -= transfer[1].len;
		i++;
	}
#endif
	/*for(i=0;i<size/2;i++){
		data[i] = be16_to_cpu(data[i]);	
	}*/

	ret = copy_to_user(buf, data,size);
        //test pour ne pas faire les molloc tous le temps

		//printk("end hf8ch_read_data %d\n");//TODO

	for(doneCpt = 0; doneCpt<10;doneCpt++){
		u32_temp = hf8ch_read_register32(ADDR_CMD);
		if(((u32_temp >> 16) & 0b0000000000000001) == 0b0000000000000001){
			u32_temp |= CONTROL_DMA_DONE;
			hf8ch_write_register32(ADDR_CMD,u32_temp);
			printk("retry dma done %d %d\n",(u32_temp >> 16),doneCpt);//TODO

		}else{
			printk("dma done\n");//TODO
			flag_dma = 0;
			break;
		}
	}
	return (ret);
exit:
	return ret;
}
static ssize_t hf8ch_read_pps(struct file *filp, char *buf, size_t size, loff_t *offp){
	int ret;
	ret = 0;
	if(flag_pps == false){
		 wait_event_interruptible(ReadPPS, flag_pps != false);
	}
	flag_pps = false;
	
	mutex_lock(&mutexPps);
	ret = copy_to_user(buf, &ppsInfoFromFpga, sizeof(ppsInfoFromFpga));
	mutex_unlock(&mutexPps);

	return ret;
}
static ssize_t hf8ch_read_debug(struct file *filp, char *buf, size_t size, loff_t *offp){
	int ret,value;
	__u32 dataTemp[2];
	if(size == sizeof(__u32)*2){
		if(copy_from_user(dataTemp,buf,size)){
			pr_err("copy_from_user\n");
			return -EINVAL;
		}else{
			//pr_info("read registre = %d\n",dataTemp[1]);
			value = hf8ch_read_register32(dataTemp[1]);
			ret = copy_to_user(buf,&value,sizeof(__u32));
		}
	}else{
		pr_err("wrong size of read debug, need 2 * sizeof(__u32) instead of %d\n",size);
		ret = -1;
	}
	return ret;
}
static
ssize_t
hf8ch_read (struct file *filp, char *buf, size_t size, loff_t *offp)
{
  	ssize_t rc;
	unsigned int minor = iminor(file_inode(filp));;

	if (minor == MINOR_CAN_DAC) {
		rc = hf8ch_read_data(filp, buf, size, offp);
	}else if ( minor == MINOR_PPS) {
		rc = hf8ch_read_pps(filp, buf, size, offp);
	}else if ( minor == MINOR_REGISTRE) {
		rc = hf8ch_read_debug(filp, buf, size, offp);
	} else {
		pr_err("hf8ch_read : Wrong minor = %d\n",minor);
		return -1;
	}
  
  
	return rc;
}
__u16 *dataTempToWrite;
static ssize_t hf8ch_write_signal(struct file *filp,const char *buf, size_t size, loff_t *offp, int minor){
	struct spi_transfer transfer[2] ;
	int i, ret;
	u8 commande;
	if(size*sizeof(__u16) > (HF8CH_MAX_WRITE_READ)){
		pr_err("size*sizeof(__u16) %d > HF8CH_MAX_WRITE_READ %d\n",size*sizeof(__u16),HF8CH_MAX_WRITE_READ);
		return -EMSGSIZE;
	}
	
	if(copy_from_user(dataTempToWrite,buf,size*sizeof(__u16))){
		pr_err("copy_from_user\n");
		return -EINVAL;
	}
	for(i=0;i<size;i++){
		dataTempToWrite[i] = be16_to_cpu(dataTempToWrite[i]);
	}	
	memset(transfer, 0, sizeof(transfer));
	
	commande = CMD_WRITE(ADDR_BURST_SIGNAL_TX_DAC_1);
	transfer[0].tx_buf = &commande;
	transfer[0].len = sizeof(commande);
	transfer[1].tx_buf = dataTempToWrite;
	transfer[1].len = size*sizeof(__u16);
	ret = spi_sync_transfer(spiHandler,&transfer[0],2);
	if (ret) {
		dev_err(&spiHandler->dev,"transfer error, error: %d\n", ret);
		return -EIO;
	}
	commande = CMD_WRITE(ADDR_BURST_SIGNAL_TX_DAC_2);
	transfer[0].tx_buf = &commande;
	transfer[0].len = sizeof(commande);
	transfer[1].tx_buf = dataTempToWrite;
	transfer[1].len = size*sizeof(__u16);
	ret = spi_sync_transfer(spiHandler,&transfer[0],2);
	if (ret) {
		dev_err(&spiHandler->dev,"transfer error, error: %d\n", ret);
		return -EIO;
	}	
	return 0;
}

static ssize_t hf8ch_write_debug(struct file *filp,const char *buf, size_t size, loff_t *offp, int minor){
	int ret;
	__u32 dataTemp[2];
	if(size == sizeof(__u32)*2){
		if(copy_from_user(dataTemp,buf,size)){
			pr_err("copy_from_user\n");
			return -EINVAL;
		}else{
			//pr_info("write registre = %d and value %d\n",dataTemp[1],dataTemp[0]);
			hf8ch_write_register32(dataTemp[1],dataTemp[0]);
			ret = 0;
		}
	}else{
		pr_err("wrong size of write debug, need 2 * sizeof(__u32) instead of %d\n",size);
		ret = -1;
	}
	return ret;
}

static ssize_t hf8ch_write(struct file *filp, const char *buf, size_t size, loff_t * offp)
{
	static int test = 1;
	int ret = -1;
	unsigned int minor = iminor(file_inode(filp));

	if (minor == MINOR_CAN_DAC) {
		ret = hf8ch_write_signal(filp,buf,size,offp,minor);
		ret = size;
	} else if ( minor == MINOR_PPS) {//todo use pin reset
		if(test){
			gpio_set_value(hard_reset_fpga_gpios,fpga_reset_level);
			printk("reset hard FPGA\n");
			test = 0;
		}else{
			gpio_set_value(hard_reset_fpga_gpios,fpga_running_level);
			printk("running FPGA\n");
			test = 1;
		}
	}else if (minor == MINOR_REGISTRE){
		ret = hf8ch_write_debug(filp,buf,size,offp,minor);
	}

	return ret;
}

static
unsigned int
hf8ch_poll (struct file *filp, struct poll_table_struct *wait)
{
	int mask = 0;
	unsigned int minor = iminor(file_inode(filp));;

	if (minor == MINOR_CAN_DAC) {
		//printk("start hf8ch_poll %d\n",flag_dma);//TODO

		poll_wait (filp, &ReadData, wait);
		if (flag_dma) {
			mask = POLLIN | POLLPRI;
			flag_dma = 0;
		}
		//printk("end hf8ch_poll %d %d\n",flag_dma, mask);//TODO

	}else if (minor == MINOR_PPS) {
		poll_wait (filp, &ReadPPS, wait);
		if (flag_pps) {
			mask = POLLIN | POLLPRI;
		}
	}
	return mask;
}

static
long
hf8ch_ioctl (struct file *file, unsigned int cmd, unsigned long arg)
{
	int ret=0,int_temp;
	u16 u16_temp;
	u32 u32_temp,u32_temp2;
	u64 u64_temp;
	int temp = 0;

	switch (cmd) {
	case START_ACQUISITION:
		break;
	case GET_NB_DATA_READ:
		temp = nbEchantillion * nbChannel;
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
	case GET_CONF_DATA_READ:
		ret = copy_to_user((ConfhfCan_t *) arg, &ConfhfCan,sizeof(ConfhfCan));
		break;
	  case SET_GAIN_0:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_GAIN_CH0,u32_temp);
		break;
	  case SET_GAIN_1:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_GAIN_CH1,u32_temp);

		break;
	  case SET_GAIN_2:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_GAIN_CH2,u32_temp);

		break;
	  case SET_GAIN_3:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_GAIN_CH3,u32_temp);

		break;
	  case SET_GAIN_4:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_GAIN_CH4,u32_temp);

		break;
	  case SET_GAIN_5:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_GAIN_CH5,u32_temp);

		break;
	  case SET_GAIN_6:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_GAIN_CH6,u32_temp);

		break;
	  case SET_GAIN_7:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_GAIN_CH7,u32_temp);

		break;
	  case SET_NBR_OF_AVG:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_NBR_OF_AVG,u32_temp);

		break;
	  case  SET_DIGIT_DELAY:
		  ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		  hf8ch_write_register32(ADDR_DIGIT_DELAY,u32_temp);

		  break;
	  case  SET_PRF:
		  ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		  hf8ch_write_register32(ADDR_PRF,u32_temp);

		  break;
	  case  SET_NB_SAMPLE_TX:
		u32_temp2 = hf8ch_read_register32(ADDR_NBR_TX_SAMPLE);
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		u32_temp2 |=u32_temp;
		hf8ch_write_register32(ADDR_NBR_TX_SAMPLE,u32_temp2);

		  break;
	  case  SET_FREQ_TX :
		u32_temp2 = hf8ch_read_register32(ADDR_NBR_TX_SAMPLE);
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		u32_temp = u32_temp << 16;
		u32_temp2 |=u32_temp;
		hf8ch_write_register32(ADDR_NBR_TX_SAMPLE,u32_temp2);
		break;
	  case  SET_CHANNEL_TX:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_TX_ACTIV_CH,u32_temp);
		ConfhfCan.activatedChanelE = u32_temp;
		break;
	  case SET_NB_SAMPLE_RX:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_NBR_RX_SAMPLE,u32_temp);
		
		
		nbEchantillion = u32_temp;
		hf8ch_write_register32(ADDR_NBR_RX_SAMPLE_BYTES,nbEchantillion*nbChannel*2);
		printk("nbsample bytes = %d\n",nbEchantillion*nbChannel*2);
		break;
	case  SET_CHANNEL_RX:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_MASK_CHANNEL_RX,u32_temp);
		ConfhfCan.activatedChanelR = u32_temp;
		nbChannel = 0;
		    while (u32_temp) {
			nbChannel += u32_temp & 1;
			u32_temp >>= 1;
		    }
		hf8ch_write_register32(ADDR_NBR_RX_SAMPLE_BYTES,nbEchantillion*nbChannel*2);
		printk("channel rx bytes = %d\n",nbEchantillion*nbChannel*2);
		break;
	case  SET_FREQ_RX:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_FREQ_SAMPLE_RX,u32_temp);
		ConfhfCan.frequency = u32_temp;
		break;
	case  TRIG_SOFT:
		u32_temp = hf8ch_read_register32(ADDR_CMD);
		u32_temp |= CONTROL_BIT_TRIG_SOFT;
		hf8ch_write_register32(ADDR_CMD,u32_temp);
		break;  
	case  TRIG_EXTERNE:
		u32_temp = hf8ch_read_register32(ADDR_CMD);
		u32_temp |= CONTROL_BIT_TRIG_EXTERNE;
		hf8ch_write_register32(ADDR_CMD,u32_temp);
		break;  
	case GET_GAIN_0	:
		temp = hf8ch_read_register32(ADDR_GAIN_CH0);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
	case GET_GAIN_1	:
		temp = hf8ch_read_register32(ADDR_GAIN_CH1);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
	case GET_GAIN_2	:
		temp = hf8ch_read_register32(ADDR_GAIN_CH2);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
	case GET_GAIN_3	:
		temp = hf8ch_read_register32(ADDR_GAIN_CH3);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
	case GET_GAIN_4	:
		temp = hf8ch_read_register32(ADDR_GAIN_CH4);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
	case GET_GAIN_5	:
		temp = hf8ch_read_register32(ADDR_GAIN_CH5);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
	case GET_GAIN_6	:
		temp = hf8ch_read_register32(ADDR_GAIN_CH6);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
	case GET_GAIN_7	:
		temp = hf8ch_read_register32(ADDR_GAIN_CH7);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;

	case GET_NBR_OF_AVG	:
		temp = hf8ch_read_register32(ADDR_NBR_OF_AVG);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
	case GET_DIGIT_DELAY :
		temp = hf8ch_read_register32(ADDR_DIGIT_DELAY);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
	case GET_PRF  :
		temp = hf8ch_read_register32(ADDR_PRF);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
	case GET_NB_SAMPLE_TX :
		temp = hf8ch_read_register32(ADDR_NBR_TX_SAMPLE);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
	case GET_CHANNEL_TX :
		temp = hf8ch_read_register32(ADDR_TX_ACTIV_CH);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
		
	case GET_NB_SAMPLE_RX :
		temp = hf8ch_read_register32(ADDR_NBR_RX_SAMPLE);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
		
	case GET_PROG_DONE :
		temp = gpio_get_value(prog_done_gpios);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
		
	case GET_NB_SAMPLE_BYTES_RX :
		temp = hf8ch_read_register32(ADDR_NBR_RX_SAMPLE_BYTES);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
		
	case GET_CHANNEL_RX :
		
		temp = hf8ch_read_register32(ADDR_MASK_CHANNEL_RX);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
	case GET_FREQ_RX :
		temp = hf8ch_read_register32(ADDR_FREQ_SAMPLE_RX);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
		
    case  SET_TYPE_DECLANCHEMENT:
		ret = copy_from_user(&u32_temp2, (u32 *) arg, sizeof(u32));
		u32_temp = hf8ch_read_register32(ADDR_CMD);
		u32_temp |= u32_temp2;
		hf8ch_write_register32(ADDR_CMD,u32_temp);
		break; 
   	case  TRIG_DATE:
		u32_temp = hf8ch_read_register32(ADDR_CMD);
		u32_temp |= CONTROL_BIT_TRIG_DATE;
		u32_temp |= CONTROL_BIT_SHOT_ON_PPS;
		hf8ch_write_register32(ADDR_CMD,u32_temp);
		break;  
   	case  SAVE_CONFIG:
		u32_temp = hf8ch_read_register32(ADDR_CMD);
		u32_temp |= CONTROL_BIT_CONFIG;
		hf8ch_write_register32(ADDR_CMD,u32_temp);
		break;  
		
	case  ENABLE_BT:
		break;
	case  ENABLE_HT:
		u32_temp = hf8ch_read_register32(ADDR_CMD);
		u32_temp |= CONTROL_BIT_ENABLE_POWER;
		hf8ch_write_register32(ADDR_CMD,u32_temp);
		mdelay(1000);//attendre la charge de la capa
		break;
	case  DISABLE_BT:
		break;
	case  DISABLE_HT:
		u32_temp = hf8ch_read_register32(ADDR_CMD);
		u32_temp &= ~CONTROL_BIT_ENABLE_POWER;
		hf8ch_write_register32(ADDR_CMD,u32_temp);
		mdelay(1000);//attendre la charge de la capa
		break;	
	case  RESET_FPGA:
		u32_temp = hf8ch_read_register32(ADDR_CMD);
		u32_temp |= CONTROL_BIT_RESET;
		printk("reset = %d\n", u32_temp);
		hf8ch_write_register32(ADDR_CMD,u32_temp);
		mdelay(100);//attendre la charge de la capa
		u32_temp &= ~CONTROL_BIT_RESET;
		printk("reset = %d\n", u32_temp);
		hf8ch_write_register32(ADDR_CMD,u32_temp);
		break; 
	case  RESET_ALL_FPGA:
		gpio_set_value(hard_reset_fpga_gpios,fpga_reset_level);
		mdelay(500);
		gpio_set_value(hard_reset_fpga_gpios,fpga_running_level);
		break; 
	case SET_ADDR_0:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		printk("write addr0 = %d\n",u32_temp);
		hf8ch_write_register32(ADDR_CMD,u32_temp);	
		break;
	case GET_ADDR_0:
		temp = hf8ch_read_register32(ADDR_CMD);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		printk("read addr0 = %d\n",temp);
		break;
	case SET_LED_BLINK:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_LED_BLINK,u32_temp);	
		break;
	case GET_LED_BLINK:
		temp = hf8ch_read_register32(ADDR_LED_BLINK);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;
	case GET_AMPLITUDE_TX  :
		temp = hf8ch_read_register32(ADDR_AMPLITUDE_TX);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;	
	case  SET_AMPLITUDE_TX:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_AMPLITUDE_TX,u32_temp);	
		break;
	case GET_VERSION_PPS  :
		u16_temp = hf8ch_get_type_irq();
		if ( (u16_temp & 0b0000000000000100) == 0b0000000000000100) {
			int_temp = 1;
		}else{
			int_temp = 0;
		}
		ret = copy_to_user((int *) arg, &(int_temp),sizeof(int));
		break;	
		
		/////////////////////////////:
	case GET_DELAIS_ACQ  :
		temp = hf8ch_read_register32(ADDR_DELAIS_ACQ);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;	
	case  SET_DELAIS_ACQ:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_DELAIS_ACQ,u32_temp);	
		break;
	case GET_SYNC_PPS  :
		temp = hf8ch_read_register32(ADDR_SYNC_PPS);
		ret = copy_to_user((int *) arg, &(temp),sizeof(int));
		break;	
	case  SET_SYNC_PPS:
		ret = copy_from_user(&u32_temp, (u32 *) arg, sizeof(u32));
		hf8ch_write_register32(ADDR_SYNC_PPS,u32_temp);	
		break;
		
	case GET_CHANNEL_ACTIVATION_SEUIL :
		temp = hf8ch_read_register32(ADDR_CHANNEL_ACTIVATION_SEUIL);
		ret = copy_to_user((int *) arg, &(temp),sizeof(temp));
		break;	
	case  SET_CHANNEL_ACTIVATION_SEUIL:
		ret = copy_from_user(&temp, (int *) arg, sizeof(temp));
		hf8ch_write_register32(ADDR_CHANNEL_ACTIVATION_SEUIL,temp);	
		break;		
	case GET_SEUIL_POS :
		temp = hf8ch_read_register32(ADDR_SEUIL_POS);
		ret = copy_to_user((int *) arg, &(temp),sizeof(temp));
		break;	
	case  SET_SEUIL_POS:
		ret = copy_from_user(&temp, (int *) arg, sizeof(temp));
		hf8ch_write_register32(ADDR_SEUIL_POS,temp);	
		break;	
	case GET_SEUIL_NEG :
		temp = hf8ch_read_register32(ADDR_SEUIL_NEG);
		ret = copy_to_user((int *) arg, &(temp),sizeof(temp));
		break;	
	case  SET_SEUIL_NEG:
		ret = copy_from_user(&temp, (int *) arg, sizeof(temp));
		hf8ch_write_register32(ADDR_SEUIL_NEG,temp);	
		break;			
	case GET_PRE_TRIG :
		temp = hf8ch_read_register32(ADDR_PRE_TRIG);
		ret = copy_to_user((int *) arg, &(temp),sizeof(temp));
		break;	
	case  SET_PRE_TRIG:
		ret = copy_from_user(&temp, (int *) arg, sizeof(temp));
		hf8ch_write_register32(ADDR_PRE_TRIG,temp);	
		break;			

		////////////////////////
 	case GET_DATE_DECLANCHEMENT :
		u64_temp = hf8ch_read_register32(ADDR_DATE_DECLANCHEMENT_MSB);
		u64_temp = u64_temp << 32;
		u64_temp += hf8ch_read_register32(ADDR_DATE_DECLANCHEMENT_LSB);
		ret = copy_to_user((u64 *) arg, &(u64_temp),sizeof(u64_temp));
		break;     
		
    case  SET_DATE_DECLANCHEMENT:
        ret = copy_from_user(&u64_temp, (u64 *) arg, sizeof(u64_temp));
        u32_temp = u64_temp;
		hf8ch_write_register32(ADDR_DATE_DECLANCHEMENT_LSB,u32_temp);
		u32_temp = u64_temp >> 32;
		hf8ch_write_register32(ADDR_DATE_DECLANCHEMENT_MSB,u32_temp);
		break;
	case GET_DATE_FPGA :
		u64_temp = hf8ch_read_register32(ADDR_SECONDE_UTC_MSB);
		u64_temp = u64_temp << 32;
		u64_temp += hf8ch_read_register32(ADDR_SECONDE_UTC_LSB);
		ret = copy_to_user((u64 *) arg, &(u64_temp),sizeof(u64_temp));
		break;
	default:
		pr_warn("%s: IOCTL cmd unknown (%d)\n", DRV_NAME, cmd);
		return -1045; //ERR_SYNCHRONIZATION_IOCTL_SPECIFIED_IOCTL_COMMAND;
	}

	if (ret != 0) {
		pr_warn(KERN_WARNING "%s : Error %d during the copy to user or from\n", DRV_NAME, ret);
		return -1041; //ERR_SYNCHRONIZATION_IOCTL_COPY_OFFICIAL_GDDATA_TO_USER;
	}

	return ret;
}


	

static
int
hf8ch_gpio_init(void)
{
	int rc;
	struct device_node *np;
	printk("%s : start hf8ch_gpio_init\n",DRV_NAME);
	if(spiHandler == NULL){
			printk("%s : spiHandler == NULL\n",DRV_NAME);
			return -1;
	}
	np = spiHandler->dev.of_node;

	rc = of_property_read_u32(np, "prog-done", &prog_done_level);
	pr_err("rc = %d   prog_done_level = %d\n", rc, prog_done_level);
	if(0 > rc){
		return -1;
	}
	//récupéré prog-done-gpios dans device tree
	prog_done_gpios = of_get_named_gpio(np, "prog-done-gpios", 0);
	if (prog_done_gpios >= 0) {
		devm_gpio_request_one(&spiHandler->dev, prog_done_gpios, GPIOF_IN, "prog-done");
	}else{
		printk("error of_get_named_gpio prog-done-gpios\n");
		return -1;
	}
	rc = of_property_read_u32(np, "fpga-reset", &fpga_reset_level);
	pr_err("rc = %d   fpga_reset_level = %d\n", rc, fpga_reset_level);
	if(0 > rc){
		return -1;
	}

	rc = of_property_read_u32(np, "fpga-running", &fpga_running_level);
	pr_err("rc = %d   fpga_running_level = %d\n", rc, fpga_running_level);
	if(0 > rc){
		return -1;
	}
	hard_reset_fpga_gpios = of_get_named_gpio(np, "hard-reset-fpga-gpios", 0);
	if (hard_reset_fpga_gpios >= 0) {
		devm_gpio_request_one(&spiHandler->dev, hard_reset_fpga_gpios, GPIOF_OUT_INIT_LOW, "hard-reset-fpga");
	}else{
		printk("error of_get_named_gpio hard-reset-fpga-gpios\n");
		return -1;
	}
	//TODO enlever ça
	gpio_set_value(hard_reset_fpga_gpios,fpga_reset_level);
	mdelay(50);
	gpio_set_value(hard_reset_fpga_gpios,fpga_running_level);
	
	return 0;
}

static
void
hf8ch_gpio_exit(void)
{

}

static const struct file_operations hf8ch_fops = {
	.owner          = THIS_MODULE,
	.read           = hf8ch_read,
	.write		 = hf8ch_write,
	.release        = hf8ch_close,
	.open           = hf8ch_open,
	.poll           = hf8ch_poll,
	.unlocked_ioctl = hf8ch_ioctl,
};


static
u32
hf8ch_userspace_init(void)
{
	int rc;
	void *ptr;
	printk("%s : start hf8ch_userspace_init\n",DRV_NAME);
	rc = alloc_chrdev_region(&m_oDev, 0, NB_FILE_DESC, DRV_NAME);
	if (rc < 0) {
		pr_err("failed to allocate char device region\n");
		goto fail_chrdev;
	}

	m_hf8chMajor = MAJOR(m_oDev);

	m_oClass = class_create(THIS_MODULE, DRV_NAME);
	if (IS_ERR(m_oClass)) {
		pr_err("failed to allocate class\n");
		goto fail_class;
	}

	cdev_init(&m_oCDev, &hf8ch_fops);
	m_oCDev.owner = THIS_MODULE;

	rc = cdev_add(&m_oCDev, m_oDev, NB_FILE_DESC);//AJOUT 2
	if (rc != 0) {
		pr_err("failed to add char device %d\n", rc);
		goto fail_cdev;
	}

	ptr = device_create (m_oClass, NULL, MKDEV (m_hf8chMajor, MINOR_PPS)/*m_oDev*/, NULL, DRV_NAME_PPS);
	if (ptr == NULL) {
		pr_err("failed to create device\n");
		goto fail_device;
	}

	ptr = device_create(m_oClass, NULL, MKDEV (m_hf8chMajor, MINOR_CAN_DAC) /*m_oDev*/, NULL, DRV_NAME_HF8CH);
	if (ptr == NULL) {
		pr_err("failed to create device\n");
		goto fail_device;
	}

	ptr = device_create(m_oClass, NULL, MKDEV (m_hf8chMajor, MINOR_REGISTRE) /*m_oDev*/, NULL, DRV_NAME_REGISTRE);
	if (ptr == NULL) {
		pr_err("failed to create device\n");
		goto fail_device;
	}

	pr_debug("userspace_init succes\n");
	return 0;

fail_device:
	cdev_del(&m_oCDev);

fail_cdev:
	class_destroy(m_oClass);

fail_class:
	unregister_chrdev_region(m_oDev, 1);

fail_chrdev:
	return -1;
}




static
void
hf8ch_userspace_exit(void)
{

	// Delete its node and its entrie in "/dev"
	device_destroy (m_oClass, MKDEV (m_hf8chMajor, MINOR_PPS));
	device_destroy (m_oClass, MKDEV (m_hf8chMajor,MINOR_CAN_DAC));
	device_destroy (m_oClass, MKDEV (m_hf8chMajor, MINOR_REGISTRE));
	
	cdev_del(&m_oCDev);
	class_destroy(m_oClass);
	unregister_chrdev_region(m_oDev, 1);
}

static
int hf8ch_probe(struct spi_device *spi)
{
	pr_debug("start hf8ch_probe\n");

	spiHandler = spi;
	if (spiHandler == NULL) {
		pr_err("spiHandler == NULL\n");
		return -EIO;
	}

	return 0;
}

static int hf8ch_remove(struct spi_device *spi)
{
	return 0;
}

static const struct spi_device_id hf8ch_ids[] = {
	{ .name = "ifsttarhf8ch" },
	{},
};
MODULE_DEVICE_TABLE(spi, hf8ch_ids);

//new with dts
static const struct of_device_id hf8ch_of_match[] = {
	{
		.compatible = "ifsttar,ifsttarhf8ch",
	},
	{}
};

MODULE_DEVICE_TABLE(of, hf8ch_of_match);


static struct spi_driver hf8ch_spi_driver = {
	.driver = {
		.name   = DRV_NAME,
		.owner  = THIS_MODULE,
		.of_match_table = of_match_ptr(hf8ch_of_match),
	},
	.probe  = hf8ch_probe,
	.remove = hf8ch_remove,
	.id_table =	hf8ch_ids,
};




/*
 * Helper to init & exit subsystems: SPI, GPIO, Class & Devices
 */
static int hf8ch_spi_init(void)
{
	int status = 0;
	printk("%s : start hf8ch_spi_init\n",DRV_NAME);

	status = spi_register_driver(&hf8ch_spi_driver);
	if(status < 0){
		pr_err("status = spi_register_driver(&hf8ch_spi_driver) = %d\n", status);
	}
	return status;
}

static void hf8ch_spi_exit(void)
{
	spi_unregister_driver(&hf8ch_spi_driver);
}


static int hf8c_chekFPGA(void){
	int i,isBoot;	
	for(i=0;i<10;i++){
		mdelay(1000);
		isBoot = gpio_get_value(prog_done_gpios);
		if(!isBoot){
			printk("value boot = %d try = %d\n",isBoot,i);
		}else{
			printk("FPGA prob : value boot = %d try = %d\n",isBoot,i);
			break;
		}
	}
	if(!isBoot){
		pr_err("%s : prog done to 0\n",DRV_NAME);
		//return -1;
	}
	mdelay(2000); //attendre que le FPGA est le temp de décoder les tram GPS
	return 0;
}

static int basicProcShow(struct seq_file *m, void *v)
{
	u64 u64_temp;
	u32 u32_temp;
	u64_temp = hf8ch_read_register32(ADDR_SECONDE_UTC_MSB);
	u64_temp = u64_temp << 32;
	u64_temp += hf8ch_read_register32(ADDR_SECONDE_UTC_LSB);
	seq_printf(m, "date FPGA = %llu\n",u64_temp);
	u32_temp = hf8ch_read_register32(ADDR_CMD);
	seq_printf(m, "registre intéruption = %X (%d)\n",u32_temp,u32_temp);
	return 0;
}

static int basicProcOpen(struct inode *inode, struct file *file)
{
	return single_open(file, basicProcShow, NULL);
}

static const struct proc_ops basic_proc_ops =
{
	.proc_open = basicProcOpen,
	.proc_read = seq_read,
	.proc_lseek = seq_lseek,
	.proc_release = single_release,
};



/*
 *  Module Init & Exit
 */
static int __init hf8ch_init (void)
{
	thread_read_running = true;
        //test pour ne pas faire les molloc tous le temps
	data = (__u16*)kmalloc(MAX_DATA_READ_KERNEL*8* sizeof(u16),GFP_DMA);
	dataTempToWrite = (__u16*)kmalloc(HF8CH_MAX_WRITE_READ* sizeof(u16),GFP_DMA);
	basicProcfsEntry = proc_create(DRV_NAME,0666,NULL,&basic_proc_ops);
	if(!basicProcfsEntry)
	{
		printk("%s : Procfs creation failed\n",DRV_NAME);
		goto fail_probe;
	}
	
	if (hf8ch_spi_init() < 0) {
		pr_err("%s : failed to initialize spi\n",DRV_NAME);
		goto fail_spi;
	}

	if (hf8ch_gpio_init() < 0) {
		pr_err("%s : failed to initialize GPIO\n",DRV_NAME);
		goto fail_gpio;
	}

	if (hf8ch_userspace_init() < 0) {
		pr_err("%s : failed to initialize userspace\n",DRV_NAME);
		goto fail_userspace;
	}
	
	if(hf8c_chekFPGA()< 0){
		pr_err("%s : failed to chekFPGA\n",DRV_NAME);
		goto fail_chek;
	}
	
	hf8ch_init_irq();

	task_read_it = kthread_create(&thread_read_it, NULL, "irq_dealer");
	wake_up_process(task_read_it);

	pr_notice(DRV_NAME " : ready\n");

	return 0;


fail_chek:
	hf8ch_userspace_exit();

fail_userspace:
	hf8ch_gpio_exit();

fail_gpio:
	hf8ch_spi_exit();
	
fail_spi: 
	remove_proc_entry(DRV_NAME, NULL);
fail_probe:
	return -1;
}


static void __exit hf8ch_exit(void){
        //test pour ne pas faire les molloc tous le temps
	kfree(data);
	kfree(dataTempToWrite);
	pr_debug("hf8ch_exit\n");
	/** remove proc entry **/
	remove_proc_entry(DRV_NAME, NULL);

	// Ensure all is destroy, thread / mutex ...
	thread_read_running = false;
	kthread_stop(task_read_it);

	pr_debug("hf8ch_userspace_exit\n");
	hf8ch_userspace_exit();

	pr_debug("hf8ch_gpio_exit\n");
	hf8ch_gpio_exit();

	pr_debug("hf8ch_spi_exit\n");
	hf8ch_spi_exit();

	pr_debug("hf8ch_exit_irq\n");
	hf8ch_irq_exit();

	pr_notice(DRV_NAME " : unloaded\n");
}
module_init (hf8ch_init);
module_exit (hf8ch_exit);
