#ifndef IFSTTAR_8CH_DRV_H
#define IFSTTAR_8CH_DRV_H

#include <linux/ioctl.h>
#include <linux/types.h>


typedef struct _ConfhfCan ConfhfCan_t;
struct _ConfhfCan {
	__u32 Chanel;
	__u32 typeAcq;
	__u32 activatedChanelE;
	__u32 activatedChanelR;
	__u32 frequency;
	__u64 timefirst;
	__u32 cptfirst;
	__u64 timelast;
	__u32 cptlast;
} __attribute__ ((packed));

typedef struct _ppsInfos ppsInfos_t;
struct _ppsInfos {
    uint64_t time;          /**< \brief Unix timestamp of the described informations */
    uint32_t period;        /**< \brief Size of the period in timer step */
    int32_t qerr;       /**< \brief Offset of the PPS in the end of the second */
};

#define HF8CH_MODE_RX 1
#define HF8CH_MODE_RX_TX 2 

#define HF8CH_MAX_DATA_READ 1000000 //625*2*8
#define HF8CH_MAX_WRITE_READ 65536

#define DRV_NAME_PPS     "ifsttar_hf8ch-pps"
#define DRV_NAME_HF8CH   "ifsttar_hf8ch"
#define DRV_NAME_REGISTRE   "ifsttar_hf8ch-registre"

#define CAN8CH_MAGIC  'A'
#define START_ACQUISITION           _IO(CAN8CH_MAGIC, 0x01)

#define GET_NB_DATA_READ			      _IOR(CAN8CH_MAGIC, 0x02, int)
#define GET_CONF_DATA_READ			      _IOR(CAN8CH_MAGIC, 0x03, ConfhfCan_t)

#define SET_GAIN_0				          _IOW(CAN8CH_MAGIC, 0x04, int)
#define SET_GAIN_1				          _IOW(CAN8CH_MAGIC, 0x05, int)
#define SET_GAIN_2				          _IOW(CAN8CH_MAGIC, 0x06, int)
#define SET_GAIN_3				          _IOW(CAN8CH_MAGIC, 0x07, int)
#define SET_GAIN_4				          _IOW(CAN8CH_MAGIC, 0x08, int)
#define SET_GAIN_5				          _IOW(CAN8CH_MAGIC, 0x09, int)
#define SET_GAIN_6				          _IOW(CAN8CH_MAGIC, 0x0A, int)
#define SET_GAIN_7				          _IOW(CAN8CH_MAGIC, 0x0B, int)

#define SET_NBR_OF_AVG				      _IOW(CAN8CH_MAGIC, 0x0C, int)
#define SET_DIGIT_DELAY             	  _IOW(CAN8CH_MAGIC, 0x0D, int)
#define SET_PRF                     	  _IOW(CAN8CH_MAGIC, 0x0E, int)
#define SET_NB_SAMPLE_TX            	  _IOW(CAN8CH_MAGIC, 0x0F, int)
#define SET_CHANNEL_TX              	  _IOW(CAN8CH_MAGIC, 0x10, int)
#define SET_NB_SAMPLE_RX            	  _IOW(CAN8CH_MAGIC, 0x11, int)
#define GET_NB_SAMPLE_BYTES_RX      	  _IOW(CAN8CH_MAGIC, 0x12, int)
#define SET_CHANNEL_RX              	  _IOW(CAN8CH_MAGIC, 0x13, int)
#define SET_FREQ_RX                 	  _IOW(CAN8CH_MAGIC, 0x14, int)
#define TRIG_SOFT                   	  _IO(CAN8CH_MAGIC, 0x15)

#define GET_GAIN_0				          _IOW(CAN8CH_MAGIC, 0x16, int)
#define GET_GAIN_1				          _IOW(CAN8CH_MAGIC, 0x17, int)
#define GET_GAIN_2				          _IOW(CAN8CH_MAGIC, 0x18, int)
#define GET_GAIN_3				          _IOW(CAN8CH_MAGIC, 0x19, int)
#define GET_GAIN_4				          _IOW(CAN8CH_MAGIC, 0x1A, int)
#define GET_GAIN_5				          _IOW(CAN8CH_MAGIC, 0x1B, int)
#define GET_GAIN_6				          _IOW(CAN8CH_MAGIC, 0x1C, int)
#define GET_GAIN_7				          _IOW(CAN8CH_MAGIC, 0x1D, int)

#define GET_NBR_OF_AVG				      _IOW(CAN8CH_MAGIC, 0x1E, int)
#define GET_DIGIT_DELAY             	  _IOW(CAN8CH_MAGIC, 0x1F, int)
#define GET_PRF                     	  _IOW(CAN8CH_MAGIC, 0x20, int)
#define GET_NB_SAMPLE_TX            	  _IOW(CAN8CH_MAGIC, 0x21, int)
#define GET_CHANNEL_TX              	  _IOW(CAN8CH_MAGIC, 0x22, int)
#define GET_NB_SAMPLE_RX            	  _IOW(CAN8CH_MAGIC, 0x23, int)
#define GET_CHANNEL_RX              	  _IOW(CAN8CH_MAGIC, 0x25, int)
#define GET_FREQ_RX                 	  _IOW(CAN8CH_MAGIC, 0x26, int)

#define SET_TYPE_DECLANCHEMENT	           _IOW(CAN8CH_MAGIC, 0x27, int)
#define GET_TYPE_DECLANCHEMENT            _IOR(CAN8CH_MAGIC, 0x28, int)
#define SET_DATE_DECLANCHEMENT            _IOW(CAN8CH_MAGIC, 0x29, uint64_t)
#define GET_DATE_DECLANCHEMENT            _IOR(CAN8CH_MAGIC, 0x2A, uint64_t)
#define TRIG_DATE                   	  _IO(CAN8CH_MAGIC, 0x2B)
#define GET_DATE_FPGA                     _IOR(CAN8CH_MAGIC, 0x2C, uint64_t)
#define SET_FREQ_TX              	  _IOW(CAN8CH_MAGIC, 0x2D, int)
#define SAVE_CONFIG                   	  _IO(CAN8CH_MAGIC, 0x2E)
#define RESET_FPGA                   	  _IO(CAN8CH_MAGIC, 0x2F)
#define SET_AMPLITUDE_TX		          _IOW(CAN8CH_MAGIC, 0x30, int)
#define GET_AMPLITUDE_TX            	  _IOR(CAN8CH_MAGIC, 0x31, int)
#define TRIG_EXTERNE	            	  _IO(CAN8CH_MAGIC, 0x32)
#define RESET_ALL_FPGA                 	  _IO(CAN8CH_MAGIC, 0x33)
#define ENABLE_BT			  _IOW(CAN8CH_MAGIC, 0x34, int)
#define DISABLE_BT			  _IOW(CAN8CH_MAGIC, 0x35, int)
#define ENABLE_HT			  _IOW(CAN8CH_MAGIC, 0x36, int)
#define DISABLE_HT			  _IOW(CAN8CH_MAGIC, 0x37, int)
#define SET_ADDR_0			  _IOW(CAN8CH_MAGIC, 0x38, int)
#define GET_ADDR_0			  _IOR(CAN8CH_MAGIC, 0x39, int)
#define GET_PROG_DONE			  _IOR(CAN8CH_MAGIC, 0x3a, int)
//temporaire
#define SET_LED_BLINK			  _IOW(CAN8CH_MAGIC, 0x3b, int)
#define GET_LED_BLINK			  _IOR(CAN8CH_MAGIC, 0x3c, int)

#define GET_DELAIS_ACQ			   _IOR(CAN8CH_MAGIC, 0x3d, int)
#define SET_DELAIS_ACQ                    _IOW(CAN8CH_MAGIC, 0x3e, int)
#define GET_SYNC_PPS                      _IOR(CAN8CH_MAGIC, 0x3f, int)
#define SET_SYNC_PPS                      _IOW(CAN8CH_MAGIC, 0x40, int) 
#define GET_VERSION_PPS                   _IOR(CAN8CH_MAGIC, 0x41, int) 

#define	GET_CHANNEL_ACTIVATION_SEUIL	  _IOR(CAN8CH_MAGIC, 0x42, int)
#define	SET_CHANNEL_ACTIVATION_SEUIL	  _IOW(CAN8CH_MAGIC, 0x43, int)
#define	GET_SEUIL_POS			  _IOR(CAN8CH_MAGIC, 0x44, int)
#define	SET_SEUIL_POS			  _IOW(CAN8CH_MAGIC, 0x45, int)
#define	GET_SEUIL_NEG			  _IOR(CAN8CH_MAGIC, 0x46, int)
#define	SET_SEUIL_NEG			  _IOW(CAN8CH_MAGIC, 0x47, int)
#define	GET_PRE_TRIG			  _IOR(CAN8CH_MAGIC, 0x48, int)
#define	SET_PRE_TRIG			  _IOW(CAN8CH_MAGIC, 0x49, int)

#define INIT 0
#define ATOMICREAD 1
#define SAVEANDCLEAN 2

#endif  // IFSTTAR_8CH_DRV_H
