#ifndef IFSTTAR_GPSDATA_H
#define IFSTTAR_GPSDATA_H

#include <linux/ioctl.h>
#include <linux/types.h>



// Structure GpsData
typedef struct _GpsData {

	// TODO: check if deprecated ???
	//
	//char 				cLatitudeRef;				// Latitude reference
	//char 				cLongitudeRef;				// Longitude reference

	// TODO: check if necvessary and available ???
	unsigned int		iQuality;					// GPS Quality Indicator : 0 = No GPS, 1 = GPS, 2 = DGPS

	unsigned int 		uiHour; 
	unsigned int 		uiMinute;
	unsigned int 		uiSeconde;
	unsigned int 		uiYear;
	unsigned int 		uiMonth;
	unsigned int 		uiDay;
	__u64 				oTimeTTimeInSeconds;	// Time in seconds since the 01/01/1970
	double 				dLatitude;				// Latitude
	double 				dLongitude;				// Longitude
	double 	     		dAltitude;				// TODO LL add Altitude in structure from GPSD
	unsigned int 		uiSatelliteUsed;		// Used satellites number
	unsigned int 		uiSatelliteVisible;
	double       		dHdop;
	double				dVdop;
	double 	     		dSpeed;
	double 				dMajor_deviation;
	double 				dMinor_deviation;

	unsigned int uiTimerStep;		// Microsecond
	unsigned int uiPeriodaverage;
	//int iQuartzError;				// Quartz error in µseconds
	//int iDriverMode;				// Driver running mode

	// Survey-In & Fixed Mode parameters
	bool                isSurveyinMode;
	bool                isFixedMode;
	unsigned int        surveyinObservationTime;    // seconds
	int                 ECEFMeanX;                  // cm
	int                 ECEFMeanY;                  // cm
	int                 ECEFMeanZ;                  // cm
	int                 ECEFMeanV;                  // mm^2
	int                 qErr;                       // ps

	unsigned int	uiTow	;		    // ms  GPS Millisecond Time of Week
	unsigned int	uiAcc	;		    // cm  3D Position Accuracy Estimate
	unsigned int	uiNumSV	;		    // Number of SVs used in Nav Solution
	unsigned int    uiNumCh	;		    // Number of channels
	
	
	struct  {
		unsigned int  uiIdSat;
		unsigned int  uiQuality;
		int           iElev;
		int 		  iAzim;
	} sv[32];

	// GPS Status
	unsigned int        timeAccuracy;
} GpsData;

#endif /* IFSTTAR_GPSDATA_H */
