#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/interrupt.h>

#include "ifsttar-GpsData.h"
#include "ifsttar_sync.h"

#define DRV_NAME		        "pegase-sync-drv" 
#define NB_READ_MULT_2			20

struct pegase_sync_dev {
	int pps_irq;
	struct platform_device *pdev;
};

static int flag_read_tab[NB_READ_MULT_2] = {0};
static int readFront = 0, readBack = 0;
static DEFINE_MUTEX(readBuffer);
static DECLARE_WAIT_QUEUE_HEAD (Read_wait);
GpsData m_oGPS_DATA_TEMPORARY;
GpsData m_oGPS_DATA_OFFICIAL;
static int realPps = 0;
static int changeDate = 0;

static LIST_HEAD(inode_list);
static DEFINE_MUTEX(inode_list_lock);

static int pegase_sync_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int pegase_sync_close(struct inode *inodep, struct file *filp)
{

    return 0;
}


static ssize_t pegase_sync_read(struct file *filp, char *buf, size_t size, loff_t * offp)
{
	static int iRet;
	int addrToWait;	
	if (mutex_lock_interruptible(&readBuffer)) {
		iRet = -ERESTARTSYS;
	}
	addrToWait = readBack;
	readBack = (readBack+1)%NB_READ_MULT_2;
	// If the read hasn't been unlocked
	mutex_unlock(&readBuffer);
	if (flag_read_tab[addrToWait] == 0) {
		// Wait for a read unlock
		wait_event_interruptible_timeout(Read_wait, flag_read_tab[addrToWait] != 0, HZ * 3);

	}
	if (mutex_lock_interruptible(&readBuffer)) {
		iRet = -ERESTARTSYS;
	}
	flag_read_tab[addrToWait] = 0;
	readFront = (readFront+1)%NB_READ_MULT_2;
	mutex_unlock(&readBuffer);
	if (signal_pending(current)){
		return -EINTR;
	}

	iRet = copy_to_user(buf,&realPps,sizeof(int));
	return iRet;
}
static 
GpsData 
EXPORT_GD_DATA_FROM_SYNCHONIZATION_DRIVER (void)
{
	return m_oGPS_DATA_OFFICIAL;
}
EXPORT_SYMBOL(EXPORT_GD_DATA_FROM_SYNCHONIZATION_DRIVER);



long pegase_sync_ioctl(struct file *filp, unsigned int cmd, unsigned long arg) {

	int ret=0;
	switch(cmd) {

		// SET the whole GpsData each second from CDaemonGps
		case SYNC_IOCTL_SET_GPS_DATA:
			ret=copy_from_user(&m_oGPS_DATA_TEMPORARY, (GpsData *) arg, sizeof(GpsData));
			//printk(" m_oGPS_DATA_TEMPORARY %d \n",m_oGPS_DATA_TEMPORARY.uiYear);
			// If the an error occured during the copy
			if (ret !=0) {
				// Send back an error code
				return -ret;
			} 
			break;

		// GET the whole GpsData each
		case SYNC_IOCTL_GET_OFFICIAL_GD_DATA:
			EXPORT_GD_DATA_FROM_SYNCHONIZATION_DRIVER ();
			//printk(" %u %u %u \n",oActualGDData.uiTimerStep,oActualGDData.uiPeriodaverage,oActualGDData.oData.oTimeTTimeInSeconds);
		
			ret = copy_to_user((GpsData *) arg, &(m_oGPS_DATA_OFFICIAL),sizeof(GpsData));
		
			// If the an error occured during the copy
			if (ret !=0) {
				printk(KERN_WARNING "%s%s : Error %d during the copy of the GD Data to the user\n", DRV_NAME, __func__, ret);
				// Send back an error code
				return -ret;
			} 
			break;
			
		case SYNC_IOCTL_SET_SYSTEMTIME:		
		
			changeDate = 1 ;
			break;	
		
		default :
			printk(KERN_WARNING "%s%s : IOCTL unrecognized : %d \n", DRV_NAME, __func__, cmd);
			// Send back an error code
			return -1;
	}
	return 0;
}

static irqreturn_t pegase_sync_irq (int irq, void *dev_id){
	int i;
	ktime_t t;
	struct timespec64 ts;
		// Si aucune heure temporaire n'a été passée depuis le dernier PPS
	if (m_oGPS_DATA_TEMPORARY.oTimeTTimeInSeconds == 123456) {
		struct tm tm;
		m_oGPS_DATA_OFFICIAL.oTimeTTimeInSeconds++;
		time64_to_tm(m_oGPS_DATA_OFFICIAL.oTimeTTimeInSeconds,0,&tm );
		//printk("m_oGPS_DATA_OFFICIAL.oTimeTTimeInSeconds %d \n",m_oGPS_DATA_OFFICIAL.oTimeTTimeInSeconds);
		m_oGPS_DATA_OFFICIAL.uiHour           = tm.tm_hour;
		m_oGPS_DATA_OFFICIAL.uiMinute         = tm.tm_min;
		m_oGPS_DATA_OFFICIAL.uiSeconde        = tm.tm_sec;
		m_oGPS_DATA_OFFICIAL.uiYear           = 1900 + tm.tm_year;
		m_oGPS_DATA_OFFICIAL.uiMonth          = tm.tm_mon +1;
		m_oGPS_DATA_OFFICIAL.uiDay            = tm.tm_mday;
		//printk("pas de gps %d:%d:%d %d %d %d \n",m_oGPS_DATA_OFFICIAL.uiHour,m_oGPS_DATA_OFFICIAL.uiMinute, m_oGPS_DATA_OFFICIAL.uiSeconde,m_oGPS_DATA_OFFICIAL.uiYear,m_oGPS_DATA_OFFICIAL.uiMonth,m_oGPS_DATA_OFFICIAL.uiDay);
	} else {
		m_oGPS_DATA_OFFICIAL = m_oGPS_DATA_TEMPORARY;
	}
	m_oGPS_DATA_TEMPORARY.oTimeTTimeInSeconds = 123456;
	if(m_oGPS_DATA_OFFICIAL.iQuality > 0){
		realPps = 1;
	}else{
		realPps = 0;
	}
	if(changeDate == 1){
			t= mktime64(m_oGPS_DATA_OFFICIAL.uiYear,m_oGPS_DATA_OFFICIAL.uiMonth,m_oGPS_DATA_OFFICIAL.uiDay,m_oGPS_DATA_OFFICIAL.uiHour,m_oGPS_DATA_OFFICIAL.uiMinute,m_oGPS_DATA_OFFICIAL.uiSeconde);
			ts.tv_sec=t;
			ts.tv_nsec=0; /* TODO idéalement, donner ici en nanosecondes le temps de préemption des IT de PPS */
			do_settimeofday64(&ts);
			changeDate = 0;
		}
		
		mutex_lock_interruptible(&readBuffer);

		if(readBack >= readFront){
			for(i = readFront; i < readBack; i++){
				flag_read_tab[i] = 1;
			}
		}else{
			for(i = readFront; i < NB_READ_MULT_2; i++){
				flag_read_tab[i] = 1;
			}
			for(i = 0; i < readBack; i++){
				flag_read_tab[i] = 1;
			}
		}
		mutex_unlock(&readBuffer);
		wake_up_interruptible (&Read_wait);
	return IRQ_HANDLED;
}

static const struct file_operations sample_fops = {
	.owner 			= 	THIS_MODULE,
	.open  			=   pegase_sync_open ,
	.read           =   pegase_sync_read,
	.release 		=   pegase_sync_close,
	.unlocked_ioctl = 	pegase_sync_ioctl,
};

struct miscdevice sample_device = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = DRV_NAME,
    .fops = &sample_fops,
};

static int pegase_sync_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct pegase_sync_dev *syncdev;
	int err = 0;

	syncdev = devm_kzalloc(dev, sizeof(*syncdev), GFP_KERNEL);
	if (!syncdev)
		return -ENOMEM;

	syncdev->pdev = pdev;
	platform_set_drvdata(pdev, syncdev);


	syncdev->pps_irq = platform_get_irq(pdev, 0);
	if(syncdev->pps_irq<0){
		dev_err(dev,"irq not find in dts %d",syncdev->pps_irq);
		return syncdev->pps_irq;
	}

	err = devm_request_threaded_irq(dev, syncdev->pps_irq, NULL,pegase_sync_irq,IRQF_TRIGGER_RISING| IRQF_ONESHOT,DRV_NAME, NULL);
	if (err) {
		dev_err(dev, "could request pps irq: %d", err);
		return err;
	}
	return 0;
}
static
int pegase_sync_remove(struct platform_device *pdev)
{
	int Result = 0;
	return Result;
}

static const struct of_device_id ifsttar_sync_of_match[] = {
    { .compatible   = "ifsttar,sync" },
    {},
};
MODULE_DEVICE_TABLE(of, ifsttar_sync_of_match);

static struct platform_driver ifsttar_sync_platform_driver = {
    .driver = {
        .name   = DRV_NAME,
        .owner  = THIS_MODULE,
        .of_match_table = ifsttar_sync_of_match,
    },
    .probe  = pegase_sync_probe,
    .remove = pegase_sync_remove,
};


static int __init misc_init(void)
{
    int error;

    error = misc_register(&sample_device);
    if (error) {
        pr_err("can't misc_register :(\n");
        return error;
    }
	error = platform_driver_register(&ifsttar_sync_platform_driver);
	if (error) {
        pr_err("can't platform_register :(\n");
        return error;
    }
    pr_info("I'm in\n");
    return 0;
}

static void __exit misc_exit(void)
{
    misc_deregister(&sample_device);
	platform_driver_unregister(&ifsttar_sync_platform_driver);
}



module_init(misc_init)
module_exit(misc_exit)

MODULE_DESCRIPTION("Deliver UTC time synchronized from GPS");
MODULE_AUTHOR("Arthur Bouche <arthur.bouche@univ-eiffel.fr>");
MODULE_LICENSE("GPL");
MODULE_VERSION ("1");
