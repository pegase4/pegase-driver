//----------------------------------------------------------------------------------------------//
//                                                                                              //
//      FILE		:	ifsttar-synchronization-drv.h	 										//
//      AUTHOR		:	Lemarchand Laurent							//
//      Description	:	Header file of the synchronization driver.								//
//						It defines the used structures, the IOCTLs, ...							//
//																								//
//----------------------------------------------------------------------------------------------//

#ifndef _PEGASE_SYNC_H_
#define _PEGASE_SYNC_H_

#include <linux/ioctl.h>

#include "ifsttar-GpsData.h"

// Magic number used to generate the ioctl command iD
#define SYNC_MAGIC  'X'

// Generation of the ioctl command iD using 'linux/ioctl.h'
// Command used to get the current GeoDatationData structure
#define SYNC_IOCTL_GET_OFFICIAL_GD_DATA	    _IOW(SYNC_MAGIC, 0x01, GpsData)
#define SYNC_IOCTL_SET_GPS_DATA		    _IOR(SYNC_MAGIC, 0x02, GpsData)
#define SYNC_IOCTL_SET_SYSTEMTIME                 _IO(SYNC_MAGIC, 0x03)

#endif /* IFSTTAR_SYNC_DRV_H */
